<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
-->
# Apache modules for HTTP-SASL
This project contains apache modules implementing [HTTP Authentication with SASL](https://tools.ietf.org/html/draft-vanrein-httpauth-sasl-04)
## What is HTTP Authentication with SASL?
Most application-level protocols standardise their authentication
   exchanges under the SASL framework.  HTTP has taken another course,
   and often ends up replicating the work to allow individual
   mechanisms.  This Apache implementation adopts full SASL authentication into
   HTTP.

## Apache modules
Two apache modules are implemented. Refer to  [Support Levels for Realm Crossover](http://internetwide.org/blog/2020/09/16/id-16-xover-levels.html)

 - [**mod_arpa2_sasl**](arpa2_sasl/README.md) is an implementation with an integrated sasl server
 according to level 0
 - [**mod_arpa2_diasasl**](arpa2_diasasl/README.md) is an implementation using a diameter backend server according to levels 1/2 and 1,
 - [**mod_arpa2_userdir**](arpa2_userdir/README.md) implements the `User:` header and exports envvar `LOCAL_USER` and may redirect to the user's directory
 - [**mod_arpa2_access**](arpa2_access/README.md) implements access control using ARPA2's [arpa2common](https://gitlab.com/arpa2/arpa2common/-/blob/master/README.MD) library

## Firefox native client
browser-client is a test program in conjunction with a [browser plugin](https://github.com/arpa2/http-sasl-plugin)  (currently only firefox is supported).

## CMake tests
- **t_cyrus_test_good** and **t_cyrus_test_bad** test level 0 using Cyrus SASL API
- **t_diasasl** tests level 1/2 using Cyrus SASL API
- **t_diasasl_qsasl** tests level 1/2 using qsasl API
- **t_diasasl_xsasl** tests level 1 using xsasl API

## Building apachemod

See `INSTALL.md` .

