<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
-->
# ARPA2 Pypeline Signaling

> *A simple module to signal Pypeline that Apache is ready to run free.*

Pypeline is a test system that starts programs, lets them continue and then
collects test status.  It needs to know when a program can enter *freerunning*
state, that is, the state in which the next program can start.

Typically, Apache needs to listen to its HTTP ports before this is possible.
This is possible with a simple post-fork hook.  In response to that trigger,
Apache prints the default synchronisation symbol `"--\n"` on its standard output.

**Read more:**

  * [Pypeline documentation](http://pypeline.arpa2.net/)
  * [Pypeline repository](https://gitlab.com/arpa2/pypeline)
  * [Pypeline use in ARPA2 Apache modules](https://gitlab.com/arpa2/apachemod/-/blob/master/test/CMakeLists.txt)

