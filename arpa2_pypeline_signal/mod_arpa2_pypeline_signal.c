/* Pypeline Signaling
 *
 * Pypeline is the test software used in ARPA2 projects, intended
 * to create sets of processes that communicate for test purposes,
 * conclude and then get closed down.  Synchronisation signals
 * must be sent by programs when they are ready for another stage
 * to be started.
 *
 * http://pypeline.arpa2.net/
 * https://gitlab.com/arpa2/pypeline
 *
 * This simple module informs Pypeline using its default signal
 * line "--\n" to tell it that Apache is listening to configured
 * HTTP sockets.
 * 
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdio.h>

#include "ap_config.h"
#include "ap_provider.h"
#include "httpd.h"
#include "http_config.h"
#include "http_protocol.h"


module AP_MODULE_DECLARE_DATA arpa2_pypeline_signal;


static void pypeline_signal(apr_pool_t *pchild, server_rec *s) {
	static int signaled = 0;
	if (signaled++ == 0) {
		printf("--\n");
		fflush(stdout);
	}
}


static void register_hooks(apr_pool_t *p) {
	ap_hook_child_init(pypeline_signal, NULL, NULL, APR_HOOK_LAST);
}


AP_DECLARE_MODULE(arpa2_pypeline_signal) = {
	STANDARD20_MODULE_STUFF,
	NULL,			/* dir config creater */
	NULL,			/* dir merger --- default is to override */
	NULL,			/* server config */
	NULL,			/* merge server config */
	NULL,			/* command apr_table_t */
	register_hooks		/* register hooks */
};
