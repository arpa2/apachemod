
<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
-->
# ARPA2 HTTP SASL module

> *This module `mod_arpa2_sasl` implements [HTTP Authentication with SASL](https://tools.ietf.org/html/draft-vanrein-httpauth-sasl)
> using [Cyrus SASL](http://www.cyrusimap.org/sasl/) as SASL backend.*

## Directives
This module uses apache's [mod_authn_core](https://httpd.apache.org/docs/2.4/mod/mod_authn_core.html)  and [mod_authz_core](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html) modules and introduces a new AuthType "SASL". The [Context](https://httpd.apache.org/docs/2.4/mod/directive-dict.html#Context) of these directives is "directory" or ".htaccess"
|Name|Cyrus equivalent|Description|
|--|--|--|
|`AuthType`||Should be `AuthType SASL`
|`AuthName`||This directive sets the name of the authorization realm for a directory. This realm is given to the client so that the user knows which username and password to send. `AuthName` takes a single argument; if the realm name contains spaces, it must be enclosed in quotation marks. It must be accompanied by [AuthType](https://httpd.apache.org/docs/2.4/mod/mod_authn_core.html#authtype) and [Require](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html#require) directives. Eg. `Authname "SASL realm"`
|`Require`||See mod_authz_core [Require](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html#require) directive, eg. `Require valid-user`
|`SaslRealm`||Realm of the SASL server eg. `SaslRealm "example.com"`.
|`SaslMechanisms`|[mech_list](http://www.cyrusimap.org/sasl/sasl/options.html#cmdoption-arg-mech_list)|Limit the server SASL mechanisms to those in this directive. If absent all available server mechanisms can be used. Value is a space delimited list of mechanism names eg. `SaslMechanisms "SCRAM-SHA-1 SCRAM-SHA-256 DIGEST-MD5"`.|
|`SaslDbPath`|[sasldb_path](http://www.cyrusimap.org/sasl/sasl/options.html#cmdoption-arg-sasldb_path)|Path to sasldb file. Default: /etc/sasldb2 (system dependant)

## Environment variables set on successful authentication
Taken from [Appendix A. HTTP Server Environment Variables](https://tools.ietf.org/id/draft-vanrein-httpauth-sasl-05.html#name-http-server-environment-var)
|Name|Description|
|--|--|
|`SASL_SECURE`|is only "yes" (without the quotes) when a client is authenticated to the current resource. It never has another value; it is simply undefined when not secured by SASL|
|`SASL_REALM`|is the realm for which the secure exchange succeeded. A realm is not always used, because sites only need it when there are more than one in the same name space. When undefined in the SASL flow, this variable will not be set.|
|`REMOTE_USER`|is the client identity as confirmed through SASL authentication. Its content is formatted like an email address, and includes a domain name. That domain need not be related to the web server; it is possible for a web server to welcome foreign clients.|
|`SASL_MECH`|indicates the mechanism used, and is one of the standardised SASL mechanism names. It may be used to detect the level of security.|
|`SASL_S2S`|currently unset|
|`SASL_S2S_`|currently unset|
