/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 */

#include <ap_config.h>
#include <ap_mmn.h>
#include <httpd.h>
#include <http_config.h>
#include <http_connection.h>
#include <http_core.h>
#include <http_log.h>
#include <http_vhost.h>
#include <http_request.h>
#include <apr_strings.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>
#include <sasl/saslutil.h>

#include <pcre.h>

#define SAMPLE_SEC_BUF_SIZE (2048)


#define RE_SASL_MECH "[A-Z0-9-_]{1,20}"
#define RE_MECHSTRING "\"(" RE_SASL_MECH "(?:[ ]" RE_SASL_MECH ")*)\""
#define RE_REALMSTRING "\"((?:[^\"\\\\]|\\\\.)*)\""
#define RE_BWS  "[ \\t]*"
#define RE_OWS  RE_BWS
#define RE_BASE64STRING  "\"([a-zA-Z0-9-._~+/]*=*)\""
#define RE_AUTH_PARAM \
    "(?:" \
        "([CcSs][2][CcSs])" RE_BWS "=" RE_BWS RE_BASE64STRING \
        "|" \
        "([Mm][Ee][Cc][Hh])" RE_BWS "=" RE_BWS RE_MECHSTRING \
        "|" \
        "([Rr][Ee][Aa][Ll][Mm])" RE_BWS "=" RE_BWS RE_REALMSTRING \
    ")"
#define RE_AUTH_SCHEME  "[Ss][Aa][Ss][Ll]"
#define RE_CREDENTIALS  RE_AUTH_SCHEME "(?:[ ]+(" RE_AUTH_PARAM "(?:" \
    RE_OWS "," RE_OWS RE_AUTH_PARAM ")+)?)"

#define HTTP_SERVICE_NAME "HTTP"

module AP_MODULE_DECLARE_DATA arpa2_sasl_module;

typedef struct {
    char *server_realm;
    char *mechanisms;
    char *dbpath;
    apr_hash_t* conns;
    sasl_callback_t *callbacks;
} sasl_dir_config;

typedef enum {
    cmd_saslmechanisms,
    cmd_sasl_dbpath,
    cmd_sasl_server_realm
} cmd_parts;

static void trace_nocontext(apr_pool_t *p, const char *file, int line,
                            const char *note)
{
    /*
     * Since we have no request or connection to trace, or any idea
     * from where this routine was called, there's really not much we
     * can do.  If we are not logging everything by way of the
     * EXAMPLE_LOG_EACH constant, do nothing in this routine.
     */

    ap_log_perror(file, line, APLOG_MODULE_INDEX, APLOG_NOTICE, 0, p,
                  APLOGNO(3297) "%s", note);
}

static int
sasl_my_log(void *context __attribute__((unused)),
            int priority,
            const char *message)
{
    const char *label;

    if (! message)
        return SASL_BADPARAM;

    switch (priority) {
    case SASL_LOG_ERR:
        label = "Error";
        break;
    case SASL_LOG_NOTE:
        label = "Info";
        break;
    default:
        label = "Other";
        break;
    }

    sasl_dir_config *pConfig = (sasl_dir_config *) context;
    apr_pool_t *p =  apr_hash_pool_get(pConfig->conns);
    char *note = apr_psprintf(
        p, "SASL %s: %s", label, message);
    trace_nocontext(p, __FILE__, __LINE__, note);

    return SASL_OK;
}

static int
cbgetopt(
    void *context, const char *plugin_name,
    const char *option,
    const char **result, unsigned *len)
{
    sasl_dir_config *dconf = (sasl_dir_config *) context;
    apr_pool_t *p =  apr_hash_pool_get(dconf->conns);
    char *note = apr_psprintf(
        p, "cbgetopt plugin_name: %s, option: %s", plugin_name, option);
    trace_nocontext(p, __FILE__, __LINE__, note);

    if (dconf) {
        if (!strcmp(option, "sasldb_path")) {
            if (dconf->dbpath) {
                *result = dconf->dbpath;
                if (len) {
                    *len = (unsigned) strlen(dconf->dbpath);
                }
                note = apr_psprintf(p, "sasldb_path: %s", *result);
                trace_nocontext(p, __FILE__, __LINE__, note);
            }
        } else if (!strcmp(option, "mech_list")) {
            if (dconf->mechanisms) {
                *result = dconf->mechanisms;
                if (len) {
                    *len = (unsigned) strlen(dconf->mechanisms);
                }
                note = apr_psprintf(p, "mech_list: %s", *result);
                trace_nocontext(p, __FILE__, __LINE__, note);
            }
        }
        return SASL_OK;
    }
    return SASL_FAIL;
}

static sasl_callback_t callbacks[] = {
  {
    SASL_CB_GETOPT, (sasl_callback_ft) cbgetopt, NULL
  }, {
    SASL_CB_LOG, (sasl_callback_ft) sasl_my_log, NULL
  }, {
    SASL_CB_LIST_END, NULL, NULL
  }
};

/*
 * Locate our directory configuration record for the current request.
 */
static sasl_dir_config *our_dconfig(const request_rec *r)
{
    return (sasl_dir_config *) ap_get_module_config(r->per_dir_config, &arpa2_sasl_module);
}

static void *create_sasl_dir_config(apr_pool_t *p, char *dirspec)
{
    sasl_dir_config *pConfig = apr_pcalloc(p, sizeof *pConfig);

    pConfig->conns = apr_hash_make(p);
    pConfig->callbacks = apr_pcalloc(p, sizeof callbacks);
    memcpy(pConfig->callbacks, callbacks, sizeof callbacks);
    pConfig->callbacks[0].context = pConfig;
    pConfig->callbacks[1].context = pConfig;

    char *note = apr_psprintf(p, "create_sasl_dir_config(p == %pp, dirspec == %s, pConfig = %pp, conns = %pp)", (void*) p, dirspec, pConfig, pConfig->conns);
    trace_nocontext(p, __FILE__, __LINE__, note);
    return pConfig;
}

static void *merge_auth_sasl_dir_config(apr_pool_t *p, void *basev, void *overridesv)
{
    sasl_dir_config *base = basev;
    sasl_dir_config *overrides = overridesv;
    sasl_dir_config *newconf = create_sasl_dir_config(p, "Merged configuration");
    newconf->dbpath = overrides->dbpath ? overrides->dbpath : base->dbpath;
    newconf->mechanisms = overrides->mechanisms ? overrides->mechanisms : base->mechanisms;
    newconf->conns = overrides->conns ? overrides->conns : base->conns;
    return newconf;
}

pcre *re_credentials;
pcre *re_auth_parm;

#ifdef HTTP_HACK
static sasl_http_request_t httpreq =
{
    "AUTHENTICATE", /* Method */
    NULL,           /* URI */
    NULL,           /* Empty body */
    0,              /* Zero-length body */
    0               /* Persistent cxn */
};
#endif

static const char *sasl_server_realm(cmd_parms *cmd, void *dconf, const char *val)
{
    sasl_dir_config *pConfig = (sasl_dir_config *) dconf;
    pConfig->server_realm = apr_pstrdup(cmd->pool, val);
    char *note = apr_psprintf(cmd->pool, "sasl_server_realm: pConfig = %pp, server_realm = %s", pConfig, pConfig->server_realm);
    trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
    return NULL;
}

static const char *sasl_mechanisms(cmd_parms *cmd, void *dconf, const char *val)
{
    sasl_dir_config *pConfig = (sasl_dir_config *) dconf;
    pConfig->mechanisms = apr_pstrdup(cmd->pool, val);
    char *note = apr_psprintf(cmd->pool, "sasl_mechanisms: pConfig = %pp, mechanisms = %s", pConfig, pConfig->mechanisms);
    trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
    return NULL;
}

static const char *sasl_dbpath(cmd_parms *cmd, void *dconf, const char *val)
{
    sasl_dir_config *pConfig = (sasl_dir_config *) dconf;
    pConfig->dbpath = apr_pstrdup(cmd->pool, val);
    char *note = apr_psprintf(cmd->pool, "sasl_dbpath: pConfig = %pp, dbpath = %s", pConfig, pConfig->dbpath);
    trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
    return NULL;
}

static const command_rec sasl_cmds[] = {
    AP_INIT_TAKE1("SaslRealm", sasl_server_realm, (void*)cmd_sasl_server_realm, OR_AUTHCFG,
                  "SASL Server Realm"),
    AP_INIT_TAKE1("SaslMechanisms", sasl_mechanisms, (void*)cmd_saslmechanisms, OR_AUTHCFG,
                  "SASL mechanisms"),
    AP_INIT_TAKE1("SaslDbPath", sasl_dbpath, (void*)cmd_sasl_dbpath, OR_AUTHCFG,
                  "Path to SASL database"),
    {NULL}
};

#define OVECCOUNT 30    /* should be a multiple of 3 */
static int ovector[OVECCOUNT];

static apr_hash_t* parse_authorization_header(apr_pool_t *p, const char *auth_line)
{
    apr_hash_t* hash = NULL;
    int rc = pcre_exec(
        re_credentials,    /* the compiled pattern */
        NULL,              /* no extra data - we didn't study the pattern */
        auth_line,         /* the subject string */
        strlen(auth_line), /* the length of the subject */
        0,                 /* start at offset 0 in the subject */
        0,                 /* default options */
        ovector,           /* output vector for substring information */
        OVECCOUNT          /* number of elements in the output vector */
    );
    if (rc >= 0) {
        int start_offset = 0;
        hash = apr_hash_make(p);

        for (;;) {
            rc = pcre_exec(
                re_auth_parm,      /* the compiled pattern */
                NULL,              /* no extra data - we didn't study the pattern */
                auth_line,         /* the subject string */
                strlen(auth_line), /* the length of the subject */
                start_offset,      /* start at offset 0 in the subject */
                0,                 /* default options */
                ovector,           /* output vector for substring information */
                OVECCOUNT          /* number of elements in the output vector */
            );
            if (rc > 0) {
                char *key = NULL;
                char *value = NULL;
                int i;

                for (i = 1; i < rc; i++) {
                    int substring_length = ovector[2 * i + 1] - ovector[2 * i];
                    if (substring_length > 0)
                    {
                        const char *substring_start = auth_line + ovector[2 * i];
                        if (key == NULL) {
                            key = apr_pmemdup(p, substring_start, substring_length + 1);
                            key[substring_length] = '\0';
                        } else if (value == NULL) {
                            value = apr_pmemdup(p, substring_start, substring_length + 1);
                            value[substring_length] = '\0';
                            apr_hash_set(hash, key, APR_HASH_KEY_STRING, value);
                            char *note = apr_psprintf(p, "adding ('%s', '%s')", key, value);
                            trace_nocontext(p, __FILE__, __LINE__, note);
                        }
                    }
                }
                start_offset = ovector[1];
            } else {
                trace_nocontext(p, __FILE__, __LINE__, "no (more) auth param matches");
                break;
            }
        }
    } else {
        trace_nocontext(p, __FILE__, __LINE__, "no credentials match");
    }
    return hash;
}

static int open_sasl(request_rec *r, sasl_conn_t **conn, const char **result_string, int *number_of_mechanisms)
{
    sasl_dir_config *dconf = our_dconfig(r);
    char *server_realm = dconf->server_realm;
    if (server_realm == NULL) {
        server_realm = (char *) r->hostname;
    }
    char *note = apr_psprintf(r->pool, "server_realm = %s", server_realm);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);

    int result = sasl_server_new(
        HTTP_SERVICE_NAME,
        NULL,           /* my fully qualified domain name;
                           NULL says use gethostname() */
        server_realm,            /* The user realm used for password
                           lookups; NULL means default to serverFQDN
                           Note: This does not affect Kerberos */
        NULL, NULL,     /* IP Address information strings */
        dconf->callbacks, /* Callbacks supported only for this connection */
        SASL_SUCCESS_DATA,              /* security flags (security layers are enabled
                               using security properties, separately) */
        conn
    );
    if (result_string) {
        unsigned string_length;
        result = sasl_listmech(
            *conn,  /* The context for this connection */
            NULL,  /* not supported */
            NULL,  /* What to prepend the string with */
            " ",   /* What to separate mechanisms with */
            NULL,  /* What to append to the string */
            result_string, /* The produced string. */
            &string_length, /* length of the string */
            number_of_mechanisms /* Number of mechanisms in
                                    the string */
        );
    }
    return result;
}

static void set_prop_envvar(sasl_conn_t *conn, apr_table_t *subprocess_env, int propnum, const char *envname)
{
    char *envvalue;

    if (sasl_getprop(conn, propnum, (const void**) &envvalue) == SASL_OK) {
        apr_table_setn(subprocess_env, envname, envvalue);
    } else {
        apr_table_unset(subprocess_env, envname);
    }
}

/* Determine user ID, and check if it really is that user, for HTTP
 * basic authentication...
 */
static int authenticate_sasl_user(request_rec *r)
{
    const char *current_auth = ap_auth_type(r);
    char *note = apr_psprintf(r->pool, "authenticate_sasl_user, ap_auth_type = %s", current_auth);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);

    apr_table_unset(r->subprocess_env, "SASL_SECURE");

    if (!current_auth || ap_cstr_casecmp(current_auth, "SASL")) {
        return DECLINED;
    }
    const char *realm = ap_auth_name(r);
    /* We need an authentication realm. */
    if (!realm) {
        ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, APLOGNO(01615)
                      "need AuthName: %s", r->uri);
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    r->ap_auth_type = (char*) current_auth;

    note = apr_psprintf(r->pool, "authenticate_sasl_user, realm = %s", realm);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);

    const char *auth_line;
    int result;
    int rc = PROXYREQ_PROXY == r->proxyreq
            ? HTTP_PROXY_AUTHENTICATION_REQUIRED
            : HTTP_UNAUTHORIZED;

    sasl_dir_config *dconf = our_dconfig(r);
    note = apr_psprintf(r->pool, "dconf = %pp", dconf);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);
    /* Get the appropriate header */
    auth_line = apr_table_get(
        r->headers_in,
        (PROXYREQ_PROXY == r->proxyreq) ? "Proxy-Authorization" : "Authorization"
    );
    sasl_conn_t *conn = NULL;
    char *s2s;
    char buf[SAMPLE_SEC_BUF_SIZE];

    if (auth_line) {
        note = apr_psprintf(r->pool, "auth_line = %s", auth_line);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
        apr_hash_t *hash = parse_authorization_header(r->pool, auth_line);

        char *c2s_base64 = apr_hash_get(hash, "c2s", APR_HASH_KEY_STRING);
        const char *clientin = NULL;
        unsigned clientinlen = 0;
        if (c2s_base64) {
            note = apr_psprintf(r->pool, "c2s_base64 = %s", c2s_base64);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            result = sasl_decode64(
                c2s_base64,
                (unsigned) strlen(c2s_base64),
                buf,
                SAMPLE_SEC_BUF_SIZE,
                &clientinlen);
            if (result == SASL_OK) {
                clientin = buf;
                note = apr_psprintf(r->pool, "c2s = %s", clientin);
                trace_nocontext(r->pool, __FILE__, __LINE__, note);
            }
        }
        char *mech = apr_hash_get(hash, "mech", APR_HASH_KEY_STRING);
        const char *serverout;
        unsigned serveroutlen;

        if (mech) {
            note = apr_psprintf(r->pool, "mech = %s, clientin = %s, clientinlen = %d", mech, clientin, clientinlen);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            result = open_sasl(r, &conn, NULL, NULL);
            if (result == SASL_OK) {
                apr_hash_set(dconf->conns, conn, sizeof(conn), conn);
                s2s = apr_psprintf(r->pool, "%pp", conn);
                note = apr_psprintf(r->pool, "sasl_server_new, s2s = %s, conns = %pp, conn = %pp", s2s, dconf->conns, conn);
                trace_nocontext(r->pool, __FILE__, __LINE__, note);
                result = sasl_server_start(conn, mech, clientin, clientinlen, &serverout, &serveroutlen);
            }
        } else {
            s2s = apr_hash_get(hash, "s2s", APR_HASH_KEY_STRING);
            apr_int64_t conn_int64 = apr_strtoi64(s2s, NULL, 16);
            conn = apr_hash_get(dconf->conns, (void *) conn_int64, sizeof(conn));
            note = apr_psprintf(r->pool, "existing connection, s2s = %s, conns = %pp, conn = %pp", s2s, dconf->conns, conn);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            note = apr_psprintf(r->pool, "clientin = %s, clientinlen = %d", clientin, clientinlen);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            result = sasl_server_step(conn, clientin, clientinlen, &serverout, &serveroutlen);
        }


        if (result != SASL_OK && result != SASL_CONTINUE) {
            note = apr_psprintf(r->pool, "Error starting SASL negotiation: %s", sasl_errstring(result, NULL, NULL));
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
        } else {
            note = apr_psprintf(r->pool, "result = %d, serverout = %s, serveroutlen = %d, mechanisms = %s", result, serverout, serveroutlen, dconf->mechanisms);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            unsigned int len;
            int result2 = sasl_encode64(serverout, serveroutlen, buf, SAMPLE_SEC_BUF_SIZE, &len);
            if (result2 == SASL_OK)
            {
                apr_table_setn(
                    r->err_headers_out,
                    (PROXYREQ_PROXY == r->proxyreq) ? "Proxy-Authenticate" : "WWW-Authenticate",
                    apr_psprintf(r->pool, "SASL s2c=\"%s\",s2s=\"%s\"", buf, s2s)
                );
                if (result == SASL_OK)
                {
                    const char *user;
                    r->user = "unknown";
                    if (sasl_getprop(conn, SASL_USERNAME, (const void**) &user) == SASL_OK) {
                        r->user = apr_pstrdup(r->pool, user);
                    }
                    note = apr_psprintf(r->pool, "r->user = %s", r->user);
                    trace_nocontext(r->pool, __FILE__, __LINE__, note);
                    apr_table_setn(r->subprocess_env, "SASL_SECURE", "yes");
                    set_prop_envvar(conn, r->subprocess_env, SASL_MECHNAME, "SASL_MECH");
                    char *realm = dconf->server_realm;
                    if (realm) {
                        apr_table_setn(r->subprocess_env, "SASL_REALM", realm);
                    } else {
                        apr_table_unset(r->subprocess_env, "SASL_REALM");
                    }
                    /*
                    Unusable s2s so don't set SASL_S2S
                    apr_table_setn(r->subprocess_env, "SASL_S2S", s2s);
                    */
                    apr_table_unset(r->subprocess_env, "SASL_S2S");
                    apr_table_unset(r->subprocess_env, "SASL_S2S_");
                    rc = OK;
                    /* remove cyrus sasl connection for hash table ... */
                    apr_hash_set(dconf->conns, conn, sizeof(conn), NULL);
                    /* ... and dispose it */
                    sasl_dispose(&conn);
                }
            }
        }
    } else {
        trace_nocontext(r->pool, __FILE__, __LINE__, "No authorization header");
        const char *result_string;
        int number_of_mechanisms;
        int result = open_sasl(r, &conn, &result_string, &number_of_mechanisms);
        if (result == SASL_OK) {
            sasl_dir_config *dconf = our_dconfig(r);
            apr_table_merge(
                r->err_headers_out,
                (PROXYREQ_PROXY == r->proxyreq) ? "Proxy-Authenticate" : "WWW-Authenticate",
                apr_psprintf(r->pool, "SASL mech=\"%s\",realm=\"%s\",s2s=\"\"", result_string, realm)
            );
            sasl_dispose(&conn);
        } else {
            trace_nocontext(r->pool, __FILE__, __LINE__, "error in sasl_listmech");
        }
    }
    return rc;
}

static void note_sasl_auth_failure(request_rec *r)
{
    char *note;

    note = apr_psprintf(r->pool, "note_sasl_auth_failure");
    trace_nocontext(r->pool, __FILE__, __LINE__, note);
}


static int hook_note_sasl_auth_failure(request_rec *r, const char *auth_type)
{
    char *note;
    note = apr_psprintf(r->pool, "hook_note_sasl_auth_failure, auth_types = %s", auth_type);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);
    if (ap_cstr_casecmp(auth_type, "SASL"))
        return DECLINED;

    note_sasl_auth_failure(r);
    return OK;
}

static void check_re(apr_pool_t *p, const pcre *re, const char *error, int erroffset)
{
    /* Compilation failed: print the error message and exit */
    if (re == NULL)
    {
        char *note = apr_psprintf(p, "PCRE compilation failed at offset %d: %s\n", erroffset, error);
        trace_nocontext(p, __FILE__, __LINE__, note);
    }
}

#ifdef DEMO_SASL_GLOBAL_LISTMECH
void print_mechs(apr_pool_t *p)
{
    const char **mechlist = sasl_global_listmech();
    char *mech;
    trace_nocontext(p, __FILE__, __LINE__, "print_mechs");

    while ((mech = *mechlist++) != NULL) {
        char *note = apr_psprintf(p, "mech: %s", mech);
        trace_nocontext(p, __FILE__, __LINE__, note);
    }
}
#else
#define print_mechs(p)
#endif

static void arpa2_sasl_fini(apr_pool_t *p)
{
    trace_nocontext(p, __FILE__, __LINE__, "arpa2_sasl_fini");
    sasl_server_done();
}

static void arpa2_sasl_init(apr_pool_t *p, server_rec *s)
{
    const char *error;
    int erroffset;

    apr_pool_cleanup_register(p, p, (void*) arpa2_sasl_fini, apr_pool_cleanup_null);
    re_credentials = pcre_compile(
        RE_CREDENTIALS, /* the pattern */
        0,              /* default options */
        &error,         /* for error message */
        &erroffset,     /* for error offset */
        NULL            /* use default character tables */
    );
    check_re(p, re_credentials, error, erroffset);
    re_auth_parm = pcre_compile(
        RE_AUTH_PARAM,  /* the pattern */
        0,              /* default options */
        &error,         /* for error message */
        &erroffset,     /* for error offset */
        NULL            /* use default character tables */
    );
    check_re(p, re_auth_parm, error, erroffset);

    int result = sasl_server_init(NULL, NULL);
    if (result == SASL_OK) {
        trace_nocontext(p, __FILE__, __LINE__, "sasl_server_init succeeded");
        print_mechs(p);
    }
}

static void register_hooks(apr_pool_t *p)
{
    ap_hook_child_init(arpa2_sasl_init, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_check_authn(authenticate_sasl_user, NULL, NULL, APR_HOOK_MIDDLE,
                        AP_AUTH_INTERNAL_PER_CONF);
    //ap_hook_note_auth_failure(hook_note_sasl_auth_failure, NULL, NULL,
    //                          APR_HOOK_MIDDLE);
}

AP_DECLARE_MODULE(arpa2_sasl) = {
    STANDARD20_MODULE_STUFF,
    create_sasl_dir_config,  /* create per-directory config structure */
    NULL,                    /* merge per-directory config structures */
    NULL,                    /* create per-server config structure */
    NULL,                    /* merge per-server config structures */
    sasl_cmds,               /* command apr_table_t */
    register_hooks           /* register hooks */
};
