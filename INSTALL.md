# Build and Install Instructions

> These are instructions for building and installing ARPA2 components
> by hand. If your operating system has packages available for the
> components, you are advised to use those.

## In A Nutshell

Like all ARPA2 (sub-)projects, apachemod has a three-stage build:
 - run `cmake` with suitable arguments
 - run the build-system that is generated
 - run the install step that is generated

For Windows, this is probably different (e.g. all contained within Visual
Studio. On UNIX-like systems, to spell it out,

```
mkdir build
cd build
cmake ..
make
make install
```

This creates a separate build directory (inside the source checkout; you
can also do it somewhere else entirely), runs `cmake` telling it where
the sources are. The default generator for CMake is Makefiles, so
then `make` is the tool to do the build and install steps.

## Dependencies

apachemod uses [CMake](https://cmake.org/) to build; you will need at least
version 3.13. When running cmake, missing dependencies are reported.

### Required

The following third-party software is needed to build apachemod:
 - *Apxs*, the build-tool for Apache modules.
   Debian package: apache2-dev
   FreeBSD package: www/apache24
   openSUSE package:
 - *Cyrus-SASL2*, libraries for SASL.
   Debian package: libsasl2-dev
   FreeBSD package: security/cyrus-sasl2
   openSUSE package:
 - *freeDiameter*, a Diameter-protocol implementation.
   Debian package:
   FreeBSD package: net/freediameter
   openSUSE package:
 - *pcre*, perl-compatible regular expressions library.
   Debian package: libpcre3-dev
   FreeBSD package: devel/pcre
   openSUSE package:
 - *com_err*, error-messages library.
   Debian package: comerr_dev
   FreeBSD package: security/krb5

The following ARPA2 software is needed to build apachemod.
These are listed in recommended dependency order, if you are
building the entire stack.
 - *ARPA2CM*, the cmake-support modules for the ARPA2 stack.
 - *arpa2common*, basic libraries and development convenience.
 - *quick-mem*, memory-management libraries.
 - *quick-der*, DER (data encoding) wrangling libraries.
 - *quick-sasl*, wrappers around SASL.
 - *kip*, encrypted document exchange.

### Recommended

The following third-party software is recommended when building apachemod,
for the full range of features and documentation:
 - *Doxygen* produces browsable documentation.
   Debian package:
   FreeBSD package: devel/doxygen
   openSUSE package:


## Options

apachemod has the following options:
 - *DEBUG* Sets compilation flags suitable for debugging the project.
