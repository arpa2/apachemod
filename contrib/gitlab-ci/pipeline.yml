# Pipeline Configuration Reference:
# https://docs.gitlab.com/ee/ci/yaml/
# https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-gitlab-ciyml
#
# From: https://www.youtube.com/watch?v=Jav4vbUrqII

#
# SETTINGS:
#  - MKHERENAME names the build script in our "mkhere" ports tree
#
variables:
  MKHERENAME: apachemod

#
# SETTINGS:
#  - list the stages to go through
#  - these occur as "stage: xxx" in each job
#  - stage "website" can deploy documents on GitLab Pages
#  - stage "pythonpkg" can run setup.py and deliver Python packages
#
stages:
  - build
  # to actually run the "test" stage, add jobs
  - test
  # to actually run the "website" stage, remove "# " from the "pages:" job below
  - website
  # to actually run the "pythonpkg" stage, remove "# " from the "python:" job below
  - pythonpkg

#
# SETTINGS:
#  - name the image or URL to load as a basis
#  - the before_script relates to this
#
image: gcc

#
# Prepare the image to run at least /mkhere and ARPA2CM
#  - The purpose is to reach a common building platform
#  - This is mostly done with portable code
#  - The same things can be tested locally
#  - Clones current git repo into the mkhere for this SHA hash
#
before_script:
  - git clone --depth 1 --single-branch https://gitlab.com/arpa2/mkhere /mkhere
  - git -C /mkhere log --color=always
  - /mkhere/ospackages.sh  osupdate ossetup
  - mkdir -p /dl/${MKHERENAME}-${CI_COMMIT_SHA}
  - git -C /dl/${MKHERENAME}-${CI_COMMIT_SHA} clone --bare $(pwd)

#
# Build stage for the current project and repository
#  - Build logic can be tested locally (with "mkhere")
#  - Target for mkhere is parameterised with current SHA hash
#  - Setup os/dependencies with parameterised general code
#  - Binary dependencies can be imported from other projects
#  - Run the reuse check and report; but do not complain over it
#  - Build the system using its "mkhere" logic, usually with CMake
#  - Build documentation, if any, and deliver it (possibly empty)
#  - Export dist.tgz, docs.cpiogz, reuse.txt
#
mkhere:
  stage: build
  variables:
    TARGET_DO: "env VERSION_${MKHERENAME}=${CI_COMMIT_SHA} /mkhere/${MKHERENAME}.sh"
    MKHERE_HTML: 1
  script:
    - printf "\e[1;36m" ; $TARGET_DO envvars ; printf "\e[0m"
    - for DEP in $($TARGET_DO dependencies) ; do printf "\e[1;33mAdding dependency $DEP\e[0m\n" ; /mkhere/$DEP.sh osdepend binhave binbuild ; done
    - $TARGET_DO have osdepend bindepend
    # - $TARGET_DO reuse touch      >reuse.html || [ -z "$CI_MERGE_REQUEST_ID" -a "x$CI_COMMIT_BRANCH" != "x$CI_DEFAULT_BRANCH" ]
    - $TARGET_DO    build    tgz 4>dist.tgz
    - $TARGET_DO test            5>test.html
    - $TARGET_DO docbuild doctgz 4>docs.tgz   || true
  artifacts:
    when: always
    name: Test Results and Build Output
    paths:
      - dist.tgz
      - docs.tgz
      - test.html
      # - reuse.html

#
# Publish documentation via GitLab Pages
#  - General job, usable for any project, run on success
#  - Triggered when "stages" includes "website"
#  - Run on branch (web)master, others may preliminary inspect docs.tgz
#  - Must be setup in the project's settings; save without "Force HTTPS"
#  - Usually at http://<prjname>.arpa2.net/ ; New Domain plus DNS setup
#  - Let's Encrypt certificates seem flawed for https:// dunnowaai
#  - Setup with naming that automatically published on GitLab Pages
#  - The pipeline will show an extra "deployed" green tick on publication
#  - Failure when provided with an empty webite (but not error before)
#
pages:
  stage: website
  when: on_success
  allow_failure: true
  only:
    - master
    - webmaster
  script:
    - tar --one-top-level=public -xzvf docs.tgz
  dependencies:
    - mkhere
  artifacts:
    when: on_success
    name: Documentation "mkhere" export
    paths:
      - public
