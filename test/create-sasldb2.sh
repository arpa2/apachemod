#! /bin/sh
#
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>

# Usage:
#
#   create-sasldb2.sh <filename> <user> <password> <realm>
#
# Does no error-checking at all.

echo "$3" | saslpasswd2 -f "$1" -u "$4" "$2"
