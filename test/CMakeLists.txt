#
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>

set (UNBOUND_HOSTS "/tmp/hosts.pump")

set (sasldb "/etc/demo.sasldb2")
set (basicdb "/etc/htpasswd.dat")

set (http-realm "HTTP realm")
set (http-pixie-realm "HTTP pixie realm")

execute_process (
    COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/pypenv
    OUTPUT_VARIABLE PYPELINE_IP_LOCALHOST)

execute_process (
    COMMAND hostname
    OUTPUT_STRIP_TRAILING_WHITESPACE
    OUTPUT_VARIABLE HOSTNAME)
string(TOLOWER ${HOSTNAME} HOSTNAME)

# Find the Apache2 executable
find_program (httpd_EXECUTABLE NAMES apache2 httpd)

find_package(CURL)
find_package(OpenSSL)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/InternetWide_Identity.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/InternetWide_Identity.conf
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/setup-xsdb-document.sh.in
    ${CMAKE_CURRENT_BINARY_DIR}/setup-xsdb-document.sh
)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/setup-xsdb-comm.sh.in
    ${CMAKE_CURRENT_BINARY_DIR}/setup-xsdb-comm.sh
)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/curl.sh.in
    ${CMAKE_CURRENT_BINARY_DIR}/curl.sh
)

if (TEST_DIAMETER)
add_executable (test-diameter
    picohttpparser.c
    test-diameter.c
)
target_link_libraries (test-diameter
    pcre::pcre
    ARPA2::XoverSASL
    ARPA2::Quick-DER
    ARPA2::util-Static
)
endif ()

# These tests need libcurl to form the outgoing requests.
if (CURL_FOUND)
    add_executable (test-cyrus
        test-cyrus.c
    )
    target_link_libraries (test-cyrus
        pcre::pcre
        CURL::libcurl
        ARPA2::util-Static
        ${Cyrus-SASL2_LIBRARIES}
    )
    # Because test-cyrus doesn't link to any ARPA2 bits, but *does*
    # use includes from ARPA2CM, need to explicitly add those headers.
    target_include_directories (test-cyrus PRIVATE ${ARPA2CM_INCLUDE_DIR})

    add_executable (test-qsasl
        test-qsasl.c
    )
    target_link_libraries (test-qsasl
        pcre::pcre
        CURL::libcurl
        OpenSSL::SSL
        ARPA2::util-Static
        ARPA2::Quick-SASL_cyrus
        ARPA2::Quick-DER
        ARPA2::Quick-MEM
    )

    add_executable (test-xsasl
        test-xsasl.c
    )
    target_link_libraries (test-xsasl
        pcre::pcre
        CURL::libcurl
        OpenSSL::SSL
        ${Cyrus-SASL2_LIBRARIES}
        ARPA2::util-Static
        ARPA2::XoverSASL
        ARPA2::Quick-DER
    )
endif()

set (curl ${CMAKE_CURRENT_BINARY_DIR}/curl.sh)

set (basic-user "demo")
set (basic-password "sekreet")
set (basic-user2 "demo2")
set (basic-password2 "sekreet2")

set (basic-realm "example.com")

set (sasldb-user "demo")
set (sasldb-password "sekreet")
set (sasldb-user2 "demo2")
set (sasldb-password2 "sekreet2")
set (sasldb-realm "example.com")
set (unicorn-alias "${sasldb-user}+unicorn")
set (setall
    ENV:ARPA2_RULES_DIR=/tmp
    ENV:KIP_REALM=${sasldb-realm}
    ENV:KIP_VARDIR=${CMAKE_CURRENT_SOURCE_DIR}/vardir
    ENV:KIP_KEYTAB=${CMAKE_CURRENT_SOURCE_DIR}/bin/unicorn.keytab
    ENV:SASL_CONF_PATH=${CMAKE_CURRENT_BINARY_DIR}/bin
    ENV:KIPSERVICE_CLIENT_REALM=${sasldb-realm}
    ENV:KIPSERVICE_CLIENTUSER_LOGIN=${sasldb-user}
    ENV:KIPSERVICE_CLIENTUSER_ACL=${unicorn-alias}
    ENV:SASL_CLIENT_REALM=${sasldb-realm}
    ENV:SASL_CLIENTUSER_LOGIN=${sasldb-user}
    ENV:SASL_CLIENTUSER_ACL=${sasldb-user}
    ENV:QUICKSASL_PASSPHRASE=sekreet
    ENV:UNBOUND_HOSTS=${UNBOUND_HOSTS}
    ENV:CURL_CA_BUNDLE=${CMAKE_CURRENT_BINARY_DIR}/cacert.pem

    ENV:MOZ_DISABLE_CONTENT_SANDBOX=1
    ENV:MOZ_DISABLE_GMP_SANDBOX=1
    ENV:MOZ_DISABLE_NPAPI_SANDBOX=1
    ENV:MOZ_DISABLE_GPU_SANDBOX=1
    ENV:MOZ_DISABLE_RDD_SANDBOX=1
    ENV:MOZ_DISABLE_SOCKET_PROCESS_SANDBOX=1
)

set (setall-digestmd5
    ENV:ARPA2_RULES_DIR=/tmp
    ENV:KIP_REALM=${sasldb-realm}
    ENV:KIP_VARDIR=${CMAKE_CURRENT_SOURCE_DIR}/vardir
    ENV:KIP_KEYTAB=${CMAKE_CURRENT_SOURCE_DIR}/bin/unicorn.keytab
    ENV:SASL_CONF_PATH=${CMAKE_CURRENT_BINARY_DIR}/bin
    ENV:KIPSERVICE_CLIENT_REALM=${sasldb-realm}
    ENV:KIPSERVICE_CLIENTUSER_LOGIN=${sasldb-user}
    ENV:KIPSERVICE_CLIENTUSER_ACL=${sasldb-user}
    ENV:SASL_CLIENT_REALM=${sasldb-realm}
    ENV:SASL_CLIENTUSER_LOGIN=${sasldb-user}
    ENV:SASL_CLIENTUSER_ACL=${sasldb-user}
    ENV:QUICKSASL_PASSPHRASE=sekreet
    ENV:UNBOUND_HOSTS=${UNBOUND_HOSTS}
    ENV:CURL_CA_BUNDLE=${CMAKE_CURRENT_BINARY_DIR}/cacert.pem

    ENV:MOZ_DISABLE_CONTENT_SANDBOX=1
    ENV:MOZ_DISABLE_GMP_SANDBOX=1
    ENV:MOZ_DISABLE_NPAPI_SANDBOX=1
    ENV:MOZ_DISABLE_GPU_SANDBOX=1
    ENV:MOZ_DISABLE_RDD_SANDBOX=1
    ENV:MOZ_DISABLE_SOCKET_PROCESS_SANDBOX=1
)

set (setwrong
    ENV:KIP_REALM=${sasldb-realm}
    ENV:KIP_VARDIR=${CMAKE_CURRENT_SOURCE_DIR}/vardir
    ENV:KIP_KEYTAB=${CMAKE_CURRENT_SOURCE_DIR}/bin/unicorn.keytab
    ENV:SASL_CONF_PATH=${CMAKE_CURRENT_BINARY_DIR}/bin
    ENV:KIPSERVICE_CLIENT_REALM=${sasldb-realm}
    ENV:KIPSERVICE_CLIENTUSER_LOGIN=${sasldb-user}
    ENV:KIPSERVICE_CLIENTUSER_ACL=${unicorn-alias}
    ENV:SASL_CLIENT_REALM=${sasldb-realm}
    ENV:SASL_CLIENTUSER_LOGIN=${sasldb-user}
    ENV:SASL_CLIENTUSER_ACL=${unicorn-alias}
    ENV:QUICKSASL_PASSPHRASE=wrong
    ENV:UNBOUND_HOSTS=${UNBOUND_HOSTS}
    ENV:CURL_CA_BUNDLE=${CMAKE_CURRENT_BINARY_DIR}/cacert.pem
)

set (setwrong-digestmd5
    ENV:KIP_REALM=${sasldb-realm}
    ENV:KIP_VARDIR=${CMAKE_CURRENT_SOURCE_DIR}/vardir
    ENV:KIP_KEYTAB=${CMAKE_CURRENT_SOURCE_DIR}/bin/unicorn.keytab
    ENV:SASL_CONF_PATH=${CMAKE_CURRENT_BINARY_DIR}/bin
    ENV:KIPSERVICE_CLIENT_REALM=${sasldb-realm}
    ENV:KIPSERVICE_CLIENTUSER_LOGIN=${sasldb-user}
    ENV:KIPSERVICE_CLIENTUSER_ACL=${sasldb-user}
    ENV:SASL_CLIENT_REALM=${sasldb-realm}
    ENV:SASL_CLIENTUSER_LOGIN=${sasldb-user}
    ENV:SASL_CLIENTUSER_ACL=${unicorn-alias}
    ENV:QUICKSASL_PASSPHRASE=wrong
    ENV:UNBOUND_HOSTS=${UNBOUND_HOSTS}
    ENV:CURL_CA_BUNDLE=${CMAKE_CURRENT_BINARY_DIR}/cacert.pem
)

set (sasldb-pixie-user "demo")
set (sasldb-pixie-password "sekreet")
set (sasldb-pixie-realm "pixie.demo.arpa2.org")
set (pixie-alias "${sasldb-pixie-user}+pixie")
set (setall-pixie
    ENV:KIP_REALM=${sasldb-pixie-realm}
    ENV:KIP_VARDIR=${CMAKE_CURRENT_SOURCE_DIR}/vardir
    ENV:KIP_KEYTAB=${CMAKE_CURRENT_SOURCE_DIR}/bin/unicorn.keytab
    ENV:SASL_CONF_PATH=${CMAKE_CURRENT_BINARY_DIR}/bin
    ENV:KIPSERVICE_CLIENT_REALM=${sasldb-pixie-realm}
    ENV:KIPSERVICE_CLIENTUSER_LOGIN=${sasldb-pixie-user}
    ENV:KIPSERVICE_CLIENTUSER_ACL=${pixie-alias}
    ENV:SASL_CLIENT_REALM=${sasldb-pixie-realm}
    ENV:SASL_CLIENTUSER_LOGIN=${sasldb-pixie-user}
    ENV:SASL_CLIENTUSER_ACL=${pixie-alias}
    ENV:QUICKSASL_PASSPHRASE=sekreet
    ENV:UNBOUND_HOSTS=${UNBOUND_HOSTS}
    ENV:CURL_CA_BUNDLE=${CMAKE_CURRENT_BINARY_DIR}/cacert.pem
)

set (sethttpservicename-realmcrossover
    ENV:HTTP_SERVICE_NAME=RealmCrossOver
)

set (sethttpservicename-http
    ENV:HTTP_SERVICE_NAME=HTTP
)

set (pump ${CMAKE_BINARY_DIR}/src/kip)
set (pypeline ${CMAKE_CURRENT_SOURCE_DIR}/pypeline)
set (argv-cp ${CMAKE_CURRENT_SOURCE_DIR}/argvcp)
set (demo-hosts ${CMAKE_CURRENT_SOURCE_DIR}/demo-hosts)
set (test-diameter ${CMAKE_CURRENT_BINARY_DIR}/test-diameter)
set (test-cyrus ${CMAKE_CURRENT_BINARY_DIR}/test-cyrus)
set (test-qsasl ${CMAKE_CURRENT_BINARY_DIR}/test-qsasl)
set (test-xsasl ${CMAKE_CURRENT_BINARY_DIR}/test-xsasl)

set (kipd ${KIP_DIR}/usr/local/sbin/kipd)
set (freediameter-client
    INFILE:DIACLIEXTCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-client.conf
    SIG:TERM PYPE:NAME:freediameter-client "PYPE:FORK:.*STATE_OPEN.*"
    ${freeDiameter_EXECUTABLE} -ddc INFILE:DIACLICONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-client.conf)
set (freediameter-client2
    INFILE:DIACLIEXTCONF2:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-client2.conf
    SIG:TERM PYPE:NAME:freediameter-client2 "PYPE:FORK:.*STATE_OPEN.*"
    ${freeDiameter_EXECUTABLE} -ddc INFILE:DIACLICONF2:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-client2.conf)
set (freediameter-server
    INFILE:DIASRVEXTCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-server.conf
    SIG:TERM PYPE:NAME:freediameter-server "PYPE:FORK:.*Core.state:.2.*3.*"
    ${freeDiameter_EXECUTABLE} -ddc INFILE:DIASRVCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-server.conf)
set (freediameter-server2
    INFILE:DIASRVEXTCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-server.conf
    SIG:TERM PYPE:NAME:freediameter-server2 "PYPE:FORK:.*Core.state:.2.*3.*"
    ${freeDiameter_EXECUTABLE} -ddc INFILE:DIASRVCONF2:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-server2.conf)
set (freediameter-server-idp
    INFILE:DIASRVEXTCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-server.conf
    SIG:TERM PYPE:NAME:freediameter-server-idp "PYPE:FORK:.*Core.state:.2.*3.*"
    ${freeDiameter_EXECUTABLE} -ddc INFILE:DIASRVCONFIDP:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-server-idp.conf)

file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}${APACHE_MODULE_DIR})
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/var/run/apache2)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/var/log/apache2)

set (APACHE_ENV "APACHE_CONFDIR=${CMAKE_CURRENT_BINARY_DIR}${APACHE_CONF_DIR}")

if (EXISTS ${APACHE_MODULE_DIR}/mod_unixd.so)
    set (LOAD_MOD_UNIXD_SO "LoadModule unixd_module ${APACHE_MODULE_DIR}/mod_unixd.so")
endif ()

add_custom_target(copy-apache-modules
    ${CMAKE_COMMAND} -E copy_directory ${APACHE_MODULE_DIR} ${CMAKE_CURRENT_BINARY_DIR}${APACHE_MODULE_DIR}
)

# create and fill basicdb
add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}${basicdb}
    COMMAND htpasswd -cb ${CMAKE_CURRENT_BINARY_DIR}${basicdb} ${basic-user}@${basic-realm} ${basic-password}
    COMMAND htpasswd -b ${CMAKE_CURRENT_BINARY_DIR}${basicdb} ${basic-user2}@${basic-realm} ${basic-password2}
)

# create and fill sasldb
add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}${sasldb}
    COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/create-sasldb2.sh ${CMAKE_CURRENT_BINARY_DIR}${sasldb} ${sasldb-user} ${sasldb-password} ${sasldb-realm}
    COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/create-sasldb2.sh ${CMAKE_CURRENT_BINARY_DIR}${sasldb} ${sasldb-user2} ${sasldb-password2} ${sasldb-realm}
    COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/create-sasldb2.sh ${CMAKE_CURRENT_BINARY_DIR}${sasldb} ${sasldb-pixie-user} ${sasldb-pixie-password} ${sasldb-pixie-realm}
)

add_custom_target(basicdb
    DEPENDS ${CMAKE_CURRENT_BINARY_DIR}${basicdb}
)

add_custom_target(sasl_db2
    DEPENDS ${CMAKE_CURRENT_BINARY_DIR}${sasldb}
)

add_custom_target(mod_arpa2_sasl_test ALL
    DEPENDS copy-apache-modules sasl_db2 basicdb
)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/var/www/html/diasasl/index.html.in
    ${CMAKE_CURRENT_BINARY_DIR}/var/www/html/diasasl/index.html
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/var/www/html/diasasl2/index.html.in
    ${CMAKE_CURRENT_BINARY_DIR}/var/www/html/diasasl2/index.html
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/var/www/html/documents/docs1/doc.html.in
    ${CMAKE_CURRENT_BINARY_DIR}/var/www/html/documents/docs1/doc.html
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/var/www/html/communication/communication.html.in
    ${CMAKE_CURRENT_BINARY_DIR}/var/www/html/communication/communication.html
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/etc/apache2/apache2.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/etc/apache2/modules.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/modules.conf
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/fdcertgen.sh.in
    ${CMAKE_CURRENT_BINARY_DIR}/fdcertgen.sh
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/Hosted_Identity.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/Hosted_Identity.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/diasasl-server.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-server.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/diasasl-server2.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-server2.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/diasasl-server-idp.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-server-idp.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/diasasl-client.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-client.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/diasasl-client2.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-client2.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/diasasl-extension-server.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-server.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/diasasl-extension-client.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-client.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/diasasl-extension-client2.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-client2.conf
    @ONLY)
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/bin/acl_wl-extension.conf.in
    ${CMAKE_CURRENT_BINARY_DIR}/bin/acl_wl-extension.conf
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/native-messaging/sasl.json
    ${CMAKE_CURRENT_BINARY_DIR}/.mozilla/native-messaging-hosts/sasl.json
    @ONLY)

configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/native-messaging/sasl.sh
    ${CMAKE_CURRENT_BINARY_DIR}/native-messaging/sasl.sh
    @ONLY)

add_custom_command (OUTPUT fdsrv.cert.pem fdcli.cert.pem
    COMMAND ${CMAKE_CURRENT_BINARY_DIR}/fdcertgen.sh)

add_custom_target (fdcerts ALL
    DEPENDS fdsrv.cert.pem fdcli.cert.pem
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")

# Setup envvars ${valgrind-client} and ${valgrind-server} depending on availability of valgrind
find_program (valgrind-PROGRAM NAMES valgrind DOC "Path to optional valgrind test program")
if (NOT ${valgrind-PROGRAM} STREQUAL "valgrind-PROGRAM-NOTFOUND")
    set (valgrind-client ${valgrind-PROGRAM} --log-file=../valclient.log)
    set (valgrind-server ${valgrind-PROGRAM} --log-file=../valserver.log)
    set (valgrind-proxy  ${valgrind-PROGRAM} --log-file=../valproxy.log )
else()
    set (valgrind-client )
    set (valgrind-server )
    set (valgrind-proxy  )
endif()

set (setsasl
    ENV:SASL_CONF_PATH=${CMAKE_CURRENT_BINARY_DIR}/etc
)

set (sethome
    ENV:XAUTHORITY=$ENV{HOME}/.Xauthority
    ENV:HOME=${CMAKE_CURRENT_BINARY_DIR}
)

add_test (t_aclr_basic ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    PYPE:NAME:curl PYPE:EXIT:200 ${curl} "https://${basic-user}%40${basic-realm}:${basic-password}@${HOSTNAME}:" TCP:APACHE "/documents/docs1/doc.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_BASIC -D APACHE2_LOAD_ACLR -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    sh ${CMAKE_CURRENT_BINARY_DIR}/setup-xsdb-document.sh
)

add_test (t_aclr_basic_forbidden ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    # 147 == 403 % 256, 403 is HTTP status code "Forbidden"
    PYPE:NAME:curl PYPE:EXIT:147 ${curl} "https://${basic-user2}%40${basic-realm}:${basic-password2}@${HOSTNAME}:" TCP:APACHE "/documents/docs1/doc.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_BASIC -D APACHE2_LOAD_ACLR -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    sh ${CMAKE_CURRENT_BINARY_DIR}/setup-xsdb-document.sh
)

add_test (t_aclc_basic ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    PYPE:NAME:curl PYPE:EXIT:200 ${curl} "https://${basic-user}%40${basic-realm}:${basic-password}@${HOSTNAME}:" TCP:APACHE "/communication/communication.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_BASIC -D APACHE2_LOAD_ACLR -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    sh ${CMAKE_CURRENT_BINARY_DIR}/setup-xsdb-comm.sh
)

add_test (t_aclr_sasl ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/documents/docs1/doc.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_SASL -D APACHE2_LOAD_ACLR -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    sh ${CMAKE_CURRENT_BINARY_DIR}/setup-xsdb-document.sh
)

add_test (t_aclr_sasl_forbidden ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    PYPE:EXIT:147 ${test-cyrus} "https://${sasldb-user2}:${sasldb-password2}@${HOSTNAME}:" TCP:APACHE "/documents/docs1/doc.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_SASL -D APACHE2_LOAD_ACLR -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    sh ${CMAKE_CURRENT_BINARY_DIR}/setup-xsdb-document.sh
)

add_test (t_aclc_sasl ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/communication/communication.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_SASL -D APACHE2_LOAD_ACLR -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    sh ${CMAKE_CURRENT_BINARY_DIR}/setup-xsdb-comm.sh
)

add_test (t_sasl_cyrus_good ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl2/index.html" PYPE:POPCAT:3
    --
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_SASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
)

add_test (t_sasl_cyrus_bad ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    PYPE:EXIT:1 ${test-cyrus} "https://${sasldb-user}:${sasldb-password}-wrong@${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_SASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
)

add_test (t_sasl_cyrus_digest_md5 ${pypeline} ${setall} ${setsasl} ${sethttpservicename-http}
    # IP:DIASASL and TCP:DIASASL not used, to keep pypeline happy
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3 2 DIGEST-MD5
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_SASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
)

add_test (t_sasl_qsasl ${pypeline} ${setall} ${setpass} ${sethttpservicename-http}
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    ${test-qsasl} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3 2 DIGEST-MD5
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_SASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
)

add_test (t_sasl_xsasl ${pypeline} ${setall} ${setpass} ${sethttpservicename-http}
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    ${test-xsasl} "https://${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3 DIGEST-MD5
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_SASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    ${kipd} IP:HERE 9876 SIG:INT
    --
    ${demo-hosts} ${UNBOUND_HOSTS} kipsvc.unicorn.demo.arpa2.lab IP:HERE
    --
    "PYPE:FORK:$" cmake -E remove -f ${UNBOUND_HOSTS}
)

# Run the freediameterD with its test client
# Note: Deprecated because we cannot safely pass channel binding information;
#       Replaced with the DiaSASL protocol, which is also easier to integrate.
add_test (t_diasasl_cyrus ${pypeline} ${setall} ${setpass} ${sethttpservicename-realmcrossover}
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    IP:DIAMETER SCTP:DIAMETER
    IP:DIAMETER2 SCTP:DIAMETER2
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl2/index.html" PYPE:POPCAT:3
    --
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_DIASASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    "PYPE:FORK:$" sleep 1
    --
    ${freediameter-client2}
    --
    ${freediameter-server2}
    --
    ${freediameter-client}
    --
    ${freediameter-server}
    --
    ${kipd} IP:HERE 9876 SIG:INT
    --
    ${demo-hosts} ${UNBOUND_HOSTS} kipsvc.unicorn.demo.arpa2.lab IP:HERE
    --
    "PYPE:FORK:$" cmake -E remove -f ${UNBOUND_HOSTS}
)

add_test (t_diasasl_cyrus_digest_md5 ${pypeline} ${setall} ${setpass} ${sethttpservicename-realmcrossover}
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    IP:DIAMETER SCTP:DIAMETER
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3 1 DIGEST-MD5
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_DIASASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    "PYPE:FORK:$" sleep 1
    --
    ${freediameter-client}
    --
    ${freediameter-server}
    --
    ${kipd} IP:HERE 9876 SIG:INT
    --
    ${demo-hosts} ${UNBOUND_HOSTS} kipsvc.unicorn.demo.arpa2.lab IP:HERE
    --
    "PYPE:FORK:$" cmake -E remove -f ${UNBOUND_HOSTS}
)

add_test (t_diasasl_cyrus_scram_sha_1 ${pypeline} ${setall} ${setpass} ${sethttpservicename-realmcrossover}
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    IP:DIAMETER SCTP:DIAMETER
    ${test-cyrus} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3 1 SCRAM-SHA-1
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_DIASASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    "PYPE:FORK:$" sleep 1
    --
    ${freediameter-client}
    --
    ${freediameter-server}
    --
    ${kipd} IP:HERE 9876 SIG:INT
    --
    ${demo-hosts} ${UNBOUND_HOSTS} kipsvc.unicorn.demo.arpa2.lab IP:HERE
    --
    "PYPE:FORK:$" cmake -E remove -f ${UNBOUND_HOSTS}
)

add_test (t_diasasl_qsasl ${pypeline} ${setall} ${setpass}
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    IP:DIAMETER SCTP:DIAMETER
    ${test-qsasl} "https://${sasldb-user}:${sasldb-password}@${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3 1 DIGEST-MD5
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_DIASASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    "PYPE:FORK:$" sleep 1
    --
    ${freediameter-client}
    --
    ${freediameter-server}
    --
    ${kipd} IP:HERE 9876 SIG:INT
    --
    ${demo-hosts} ${UNBOUND_HOSTS} kipsvc.unicorn.demo.arpa2.lab IP:HERE
    --
    "PYPE:FORK:$" cmake -E remove -f ${UNBOUND_HOSTS}
)

add_test (t_diasasl_xsasl ${pypeline} ${setall} ${setpass}
    IP:DIASASL  TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    IP:DIAMETER SCTP:DIAMETER
    ${test-xsasl} "https://${HOSTNAME}:" TCP:APACHE "/diasasl/index.html" PYPE:POPCAT:3 DIGEST-MD5 75ac0c8c7bb60f41e20af98e48a66c27
    --
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_DIASASL -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
    --
    "PYPE:FORK:$" sleep 1
    --
    ${freediameter-client}
    --
    ${freediameter-server}
    --
    ${kipd} IP:HERE 9876 SIG:INT
    --
    ${demo-hosts} ${UNBOUND_HOSTS} kipsvc.unicorn.demo.arpa2.lab IP:HERE
    --
    "PYPE:FORK:$" cmake -E remove -f ${UNBOUND_HOSTS}
)

add_test (t_local_user ${pypeline} ${setall} ${setpass}
    "PYPE:FORK:$" diff FILE:john_md "${CMAKE_SOURCE_DIR}/arpa2_userdir/test_out_john_md"
    --
    "PYPE:FORK:$" curl -o FILE:john_md -H "User: jo%68n" --resolve "${HOSTNAME}:" TCP:APACHE ":" [IP]:APACHE PYPE:POPCAT:4 "https://${HOSTNAME}:" TCP:APACHE PYPE:POPCAT:2
    --
    IP:DIASASL TCP:DIASASL
    IP:DIASASL2  TCP:DIASASL2
    INFILE:APACHECONF:${CMAKE_CURRENT_BINARY_DIR}/etc/apache2/apache2.conf
    PYPE:NAME:apache2 SIG:INT ${httpd_EXECUTABLE} -D APACHE2_LOAD_USERDIR -X -c "Listen " TCP:APACHE PYPE:POPCAT:2 -f FILE:APACHECONF
)
