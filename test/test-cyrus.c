/* SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <pcre.h>

#include <unistd.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>
#include <sasl/saslutil.h>

#include <curl/curl.h>

#include <arpa2/except.h>

#include <arpa2/util/snoprintf.h>

#define RE_SASL_MECH "[A-Z0-9-_]{1,20}"
#define RE_MECHSTRING "\"(" RE_SASL_MECH "(?:[ ]" RE_SASL_MECH ")*)\""
#define RE_REALMSTRING "\"((?:[^\"\\\\]|\\\\.)*)\""
#define RE_BWS  "[ \\t]*"
#define RE_OWS  RE_BWS
#define RE_BASE64STRING  "\"([a-zA-Z0-9-._~+/]*=*)\""
#define RE_AUTH_PARAM \
    "(?:" \
        "([CcSs][2][CcSs])" RE_BWS "=" RE_BWS RE_BASE64STRING \
        "|" \
        "([Mm][Ee][Cc][Hh])" RE_BWS "=" RE_BWS RE_MECHSTRING \
        "|" \
        "([Rr][Ee][Aa][Ll][Mm])" RE_BWS "=" RE_BWS RE_REALMSTRING \
    ")"
#define RE_AUTH_SCHEME  "[Ss][Aa][Ss][Ll]"
#define RE_CREDENTIALS  RE_AUTH_SCHEME "(?:[ ]+(" RE_AUTH_PARAM "(?:" \
    RE_OWS "," RE_OWS RE_AUTH_PARAM ")+)?)"

#define OVECCOUNT 30    /* should be a multiple of 3 */

#define SAMPLE_SEC_BUF_SIZE (2048)

#define HTTP_SERVICE_NAME "HTTP"

typedef struct {
    char *mechs;
    char *realm;
    char *s2s;
    char *s2c;
} http_sasl_t;

static int ovector[OVECCOUNT];

static pcre *re_credentials;
static pcre *re_auth_parm;

static char *authname = NULL;
static char *password = NULL;

static void check_re(const pcre *re, const char *error, int erroffset)
{
    /* Compilation failed: print the error message and exit */
    if (re == NULL)
    {
        fprintf(stderr, "PCRE compilation failed at offset %d: %s\n", erroffset, error);
    }
}

static char *get_value(char *auth_line, int i) {
    int substring_length = ovector[2 * i + 1] - ovector[2 * i];
    const char *substring_start = auth_line + ovector[2 * i];
    char *value = malloc(substring_length + 1);
    memcpy(value, substring_start, substring_length);
    value[substring_length] = '\0';
    return value;
}

void parse_www_authenticate_header(char *auth_line, int auth_line_len, http_sasl_t* http_sasl)
{
    http_sasl->mechs = NULL;
    http_sasl->s2c = NULL;
    http_sasl->s2s = NULL;
    http_sasl->realm = NULL;

    int rc = pcre_exec(
        re_credentials,    /* the compiled pattern */
        NULL,              /* no extra data - we didn't study the pattern */
        auth_line,         /* the subject string */
        auth_line_len,     /* the length of the subject */
        0,                 /* start at offset 0 in the subject */
        0,                 /* default options */
        ovector,           /* output vector for substring information */
        OVECCOUNT          /* number of elements in the output vector */
    );
    if (rc >= 0) {
        int start_offset = 0;

        for (;;) {
            rc = pcre_exec(
                re_auth_parm,      /* the compiled pattern */
                NULL,              /* no extra data - we didn't study the pattern */
                auth_line,         /* the subject string */
                auth_line_len,     /* the length of the subject */
                start_offset,      /* start at offset 0 in the subject */
                0,                 /* default options */
                ovector,           /* output vector for substring information */
                OVECCOUNT          /* number of elements in the output vector */
            );
            if (rc > 2) {
                char *key = get_value(auth_line, rc - 2);
                char *value = get_value(auth_line, rc - 1);
                fprintf(stderr, "setting ('%s', '%s')\n", key, value);
                if (strcmp(key, "mech") == 0) {
                    http_sasl->mechs = value;
                } else if (strcmp(key, "s2c") == 0) {
                    http_sasl->s2c = value;
                } else if (strcmp(key, "s2s") == 0) {
                    http_sasl->s2s = value;
                } else if (strcmp(key, "realm") == 0) {
                    http_sasl->realm = value;
                }
                start_offset = ovector[1];
            } else {
                fprintf(stderr, "no (more) auth param matches\n");
                break;
            }
        }
    } else {
        printf("no credentials match\n");
    }
}

/* logging callback -- this allows plugins and the middleware to
 *  log operations they perform.
 * inputs:
 *  context     -- logging context from the callback record
 *  level       -- logging level; see above
 *  message     -- message to log
 * returns:
 *  SASL_OK     -- no error
 *  SASL_FAIL   -- error
 */
int TODO_fixed_log (void *context, int level, const char *message) {
    switch (level) {
    case SASL_LOG_ERR:
    case SASL_LOG_FAIL:
        log_error ("%s", message);
        break;
    case SASL_LOG_WARN:
        log_warning ("%s", message);
        break;
    case SASL_LOG_NOTE:
        log_notice ("%s", message);
        break;
    case SASL_LOG_DEBUG:
    case SASL_LOG_TRACE:
        log_debug ("%s", message);
        break;
    case SASL_LOG_NONE:
    case SASL_LOG_PASS:
    default:
        break;
    }
    return SASL_OK;
}


/* callback for a server-supplied user canonicalization function.
 *
 * This function is called directly after the mechanism has the
 * authentication and authorization IDs.  It is called before any
 * User Canonicalization plugin is called.  It has the responsibility
 * of copying its output into the provided output buffers.
 *
 *  in, inlen     -- user name to canonicalize, may not be NUL terminated
 *                   may be same buffer as out
 *  flags         -- not currently used, supplied by auth mechanism
 *  user_realm    -- the user realm (may be NULL in case of client)
 *  out           -- buffer to copy user name
 *  out_max       -- max length of user name
 *  out_len       -- set to length of user name
 *
 * returns
 *  SASL_OK         on success
 *  SASL_BADPROT    username contains invalid character
 */
int TODO_fixed_canon_user (sasl_conn_t *conn,
                              void *context,
                              const char *in, unsigned inlen,
                              unsigned flags,
                              const char *user_realm,
                              char *out,
                              unsigned out_max, unsigned *out_len) {
    unsigned outlen = inlen;
    if (user_realm != NULL) {
        outlen += 1 + strlen (user_realm);
    }
    if (outlen > out_max) {
        return SASL_BADPROT;
    }
    memcpy (out, in, inlen);
    if (user_realm != NULL)  {
        out [inlen] = '@';
        memcpy (out + inlen + 1, user_realm, strlen (user_realm));
    }
    *out_len = outlen;
    log_info ("Canonical user \"%.*s\" # %d < %d in realm %s is now %.*s", inlen, in, inlen, out_max, user_realm, outlen, out);
    return SASL_OK;
}

#ifdef _WIN32
static int getpath(void *context __attribute__((unused)), char ** path)
{
    if (! path)
        return SASL_BADPARAM;

    char *sasl_path = getenv("SASL_PATH");
    if (sasl_path) {
        *path = sasl_path;
    }
    log_debug ("SASL_PATH=%s", sasl_path);
    return SASL_OK;
}

static int getconfpath(void *context __attribute__((unused)), char ** path)
{
    if (! path)
        return SASL_BADPARAM;

    char *sasl_conf_path = getenv("SASL_CONF_PATH");
    if (sasl_conf_path) {
        *path = sasl_conf_path;
    }
    log_debug ("SASL_CONF_PATH=%s", sasl_conf_path);
    return SASL_OK;
}
#endif /* _WIN32 */

/* remove \r\n at end of the line */
static void chop(char *s)
{
    char *p;

    assert(s);
    p = s + strlen(s) - 1;
    if (p[0] == '\n') {
        *p-- = '\0';
    }
    if (p >= s && p[0] == '\r') {
        *p-- = '\0';
    }
}

static int getrealm(void *context __attribute__((unused)),
            int id,
            const char **availrealms,
            const char **result)
{
    static char buf[1024];

    /* paranoia check */
    if (id != SASL_CB_GETREALM) return SASL_BADPARAM;
    if (!result) return SASL_BADPARAM;

    printf("please choose a realm (available:");
    while (*availrealms) {
        printf(" %s", *availrealms);
        availrealms++;
    }
    printf("): ");

    fgets(buf, sizeof buf, stdin);
    chop(buf);
    *result = buf;

    return SASL_OK;
}

static int getauthname(
    void *context __attribute__((unused)),
    int id,
    const char **result,
    unsigned *len)
{
    /* paranoia check */
    if (!result)
        return SASL_BADPARAM;

    *result = authname;
    if (len) {
        *len = strlen(authname);
    }

    return SASL_OK;
}

static int getpassword(
    sasl_conn_t *conn,
    void *context __attribute__((unused)),
    int id,
    sasl_secret_t **psecret)
{
    size_t len;
    static sasl_secret_t *x;

    /* paranoia check */
    if (! conn || ! psecret || id != SASL_CB_PASS)
        return SASL_BADPARAM;

    len = strlen(password);

    x = (sasl_secret_t *) realloc(x, sizeof(sasl_secret_t) + len);

    if (!x) {
        return SASL_NOMEM;
    }

    x->len = len;
    strcpy((char *) x->data, password);

    *psecret = x;
    return SASL_OK;
}

/* The callbacks that can be supported (via SASL_INTERACT) */
static sasl_callback_t client_callbacks [] = {
    { SASL_CB_LOG, (int(*)(void)) TODO_fixed_log, NULL },
#ifdef _WIN32
    { SASL_CB_GETPATH, (sasl_callback_ft)&getpath, NULL },
    { SASL_CB_GETCONFPATH, (sasl_callback_ft)&getconfpath, NULL },
#endif /* _WIN32 */
    { SASL_CB_GETREALM, (sasl_callback_ft)&getrealm, NULL },
    { SASL_CB_AUTHNAME, (sasl_callback_ft)&getauthname, NULL },
    { SASL_CB_PASS, (sasl_callback_ft)&getpassword },
    { SASL_CB_CANON_USER, (int(*)(void)) TODO_fixed_canon_user, NULL },
    { SASL_CB_LIST_END, NULL, NULL }
 };

#define WWW_AUTHENTICATE "WWW-Authenticate: "
#define WWW_AUTHENTICATE_LEN 18
#define WWW_AUTHENTICATE_SIZE 4096

char www_authenticate[WWW_AUTHENTICATE_SIZE];
int  www_authenticate_len;

size_t header_callback(char *buffer, size_t size, size_t nitems, void *userdata) {
    int len = size * nitems;

    if (len >= WWW_AUTHENTICATE_LEN && len < WWW_AUTHENTICATE_SIZE - WWW_AUTHENTICATE_LEN && memcmp(buffer, WWW_AUTHENTICATE, WWW_AUTHENTICATE_LEN) == 0 ) {
        www_authenticate_len = len - WWW_AUTHENTICATE_LEN;
        memcpy(www_authenticate, buffer + WWW_AUTHENTICATE_LEN, www_authenticate_len);
    }
    return len;
}

static int http_sasl_loop(CURL *curl, sasl_conn_t *conn, const char *mechusing,const char *client, unsigned clientlen, char *s2s) {
    int rc = -1;
    char authorization[4096];
    char s2c_buf[1024];
    sasl_interact_t *client_interact = NULL;
    http_sasl_t http_sasl;

    http_sasl.s2s = s2s;
    for (;;) {
        int offset = 0;
        char *comma = "";
        offset = snoprintf(authorization, sizeof(authorization), offset, "Authorization: SASL ");
        if (mechusing != NULL) {
            offset = snoprintf(
                authorization,
                sizeof(authorization),
                offset,
                "%smech=\"%s\"",
                comma,
                mechusing
            );
            comma = ",";
            mechusing = NULL;
        }
        if (http_sasl.s2s != NULL) {
            offset = snoprintf(
                authorization,
                sizeof(authorization),
                offset,
                "%ss2s=\"%s\"",
                comma,
                http_sasl.s2s
            );
            comma = ",";
        }
        if (clientlen != 0) {
            char buf[SAMPLE_SEC_BUF_SIZE];
            unsigned int len;
            int result = sasl_encode64(client, clientlen, buf, SAMPLE_SEC_BUF_SIZE, &len);
            if (result == SASL_OK) {
                offset = snoprintf(
                    authorization,
                    sizeof(authorization),
                    offset,
                    "%sc2s=\"%.*s\"",
                    comma,
                    len,
                    buf
                );
                comma = ",";
            }
        }
        //
        // Send c2s to the server
        /* Add an Authorization header */
        fprintf(stderr, "\n\n%s\n", authorization);
        struct curl_slist *chunk = curl_slist_append(NULL, authorization);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
        www_authenticate_len = 0;
        CURLcode res = curl_easy_perform(curl);
        /* free Authorization custom header */
        curl_slist_free_all(chunk);
        if (res != CURLE_OK) {
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        } else {
            long response_code;
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
            if (www_authenticate_len == 0) {
                printf("No WWW-Authenticate header\n");
                rc = response_code != 200L;
                break;
            } else {
                parse_www_authenticate_header(www_authenticate, www_authenticate_len, &http_sasl);
                free(http_sasl.mechs);
                free(http_sasl.realm);
                //free(http_sasl.s2s);
                fprintf(stderr, "s2c base64: %s\n", http_sasl.s2c);
                unsigned int len;
                int result = sasl_decode64(http_sasl.s2c, strlen(http_sasl.s2c), s2c_buf, sizeof(s2c_buf), &len);
                free(http_sasl.s2c);
                if (result == SASL_OK) {
                    const char *server = s2c_buf;
                    unsigned serverlen = len;
                    result = sasl_client_step(
                        conn,  /* our context */
                        server,    /* the data from the server */
                        serverlen, /* its length */
                        &client_interact,  /* this should be
                                              unallocated and NULL */
                        &client,     /* filled in on success */
                        &clientlen); /* filled in on success */

                    if (result == SASL_INTERACT) {
                        /* [deal with the interactions. See below] */
                        printf("interaction, prompt: %s", client_interact->prompt);
                    } else if (result != SASL_CONTINUE) {
                        printf("result = %d\n", result);
                        sasl_dispose(&conn);
                        rc = response_code == 200 ? 0 : response_code;
                        break;
                    }
                } else {
                    printf("error in sasl_decode64\n");
                    break;
                }
            }
        }
    }
    return rc;
}

static int http_sasl_start(CURL *curl, char *realm, char *mechanism, char *s2s) {
    int rc = -1;
    int result = sasl_client_init (client_callbacks);
    if (result == SASL_OK) {
/* The SASL context kept for the life of the connection */
        sasl_conn_t *conn = NULL;
        char *service = getenv("HTTP_SERVICE_NAME");
        if (!service) {
            service = HTTP_SERVICE_NAME;
        }
        fprintf(stderr, "HTTP service name: %s\n", service);

        /* client new connection */
        result = sasl_client_new(
            service,     /* The service we are using */
            realm,      /* The fully qualified domain
                           name of the server we're
                           connecting to */
            NULL, NULL, /* Local and remote IP
                           address strings
                           (NULL disables mechanisms
                           which require this info)*/
            NULL,       /* connection-specific
                           callbacks */
            0,          /* security flags */
            &conn);     /* allocated on success */
        if (result == SASL_OK) {
            sasl_interact_t *client_interact = NULL;
            const char *mechusing;
            const char *client = NULL;
            unsigned clientlen = 0;

            do {
                result = sasl_client_start(
                    conn,             /* the same context from
                                         above */
                    mechanism,        /* the list of mechanisms
                                         from the server */
                    &client_interact, /* filled in if an
                                         interaction is needed */
                    &client,             /* filled in on success */
                    &clientlen,          /* filled in on success */
                    &mechusing);
                if (result == SASL_INTERACT)
                {
                    /* [deal with the interactions. See interactions section below] */
                    printf("interaction, prompt: %s", client_interact->prompt);
                }
            } while (result == SASL_INTERACT); /* the mechanism may ask us to fill
                                                in things many times. result is
                                                SASL_CONTINUE on success */
            if (result == SASL_CONTINUE) {
                rc = http_sasl_loop(curl, conn, mechusing, client, clientlen, s2s);
            } else {
                const char *error = sasl_errdetail(conn);
                printf("Error in sasl_client_start: %s\n", error);
            }
        } else {
            printf("Error in sasl_client_new\n");
        }
    } else {
        printf("Error in sasl_client_init\n");
    }
    return rc;
}

/* Main program.
 */
int main (int argc, char *argv []) {
    char *url;
    int rc = -1;
    char *mechanism = NULL;
    int testrun_count = 1;
    CURL *curl;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s http://user:password@host/path [number_of_runs] [mechanism]\n", argv[0]);
        return 1;
    }
    url = argv[1];
    if (argc > 2) {
        testrun_count = atoi(argv[2]);
    }
    if (argc > 3) {
        mechanism = argv[3];
    }
    printf ("--\n");
    fflush (stdout);
    curl = curl_easy_init();
    if (curl) {
        CURLcode res;
        const char *error;
        int erroffset;
        char *realm = NULL;

        CURLUcode ucode;
        CURLU *urlu = curl_url();
        ucode= curl_url_set(urlu, CURLUPART_URL, url, 0);
        if (!ucode) {
            ucode = curl_url_get(urlu, CURLUPART_HOST, &realm, 0);
            if (!ucode) {
                fprintf(stderr, "the realm is %s\n", realm);
            }
            ucode = curl_url_get(urlu, CURLUPART_USER, &authname, 0);
            if (!ucode) {
                fprintf(stderr, "the user is %s\n", authname);
            }
            ucode = curl_url_get(urlu, CURLUPART_PASSWORD, &password, 0);
            if (!ucode) {
                fprintf(stderr, "the password is %s\n", password);
            }
            curl_url_cleanup(urlu);
        }
        re_credentials = pcre_compile(
            RE_CREDENTIALS, /* the pattern */
            0,              /* default options */
            &error,         /* for error message */
            &erroffset,     /* for error offset */
            NULL            /* use default character tables */
        );
        check_re(re_credentials, error, erroffset);
        re_auth_parm = pcre_compile(
            RE_AUTH_PARAM,  /* the pattern */
            0,              /* default options */
            &error,         /* for error message */
            &erroffset,     /* for error offset */
            NULL            /* use default character tables */
        );
        check_re(re_auth_parm, error, erroffset);
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_callback);
        /* ask libcurl to show us the verbose output */
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        char *cainfo = getenv("CURL_CA_BUNDLE");
        if (cainfo) {
            curl_easy_setopt(curl, CURLOPT_CAINFO, cainfo);
        }
        for (int testrun = 0; testrun < testrun_count; testrun++) {
            if (mechanism == NULL) {
                /* remove Authorization header */
                struct curl_slist *chunk = curl_slist_append(NULL, "Authorization:");
                curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
                www_authenticate_len = 0;
                res = curl_easy_perform(curl);
                /* free Authorization header */
                curl_slist_free_all(chunk);
                /* Check for errors */
                if (res != CURLE_OK) {
                    fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
                } else {
        //          printf("WWW-Authenticate: %.*s\n", www_authenticate_len, www_authenticate);
                    if (www_authenticate_len == 0) {
                        printf("No WWW-Authenticate header\n");
                    } else {
                        http_sasl_t http_sasl;

                        parse_www_authenticate_header(www_authenticate, www_authenticate_len, &http_sasl);
                        if (http_sasl.mechs != NULL) {
                            fprintf(stderr, "mechanisms: %s\n", http_sasl.mechs);
                            rc = http_sasl_start(curl, realm, http_sasl.mechs, http_sasl.s2s);
                            free(http_sasl.mechs);
                        } else {
                            printf("no mechs\n");
                        }
                        free(http_sasl.realm);
                        free(http_sasl.s2s);
                    }
                }
            } else {
                rc = http_sasl_start(curl, realm, mechanism, NULL);
            }
        }
        if (authname) {
            curl_free(authname);
        }
        if (password) {
            curl_free(password);
        }
        curl_easy_cleanup(curl);
    } else {
        fprintf(stderr, "curl_easy_init() failed\n");
    }
    fprintf(stderr, "rc = %d\n", rc);
    return rc;
}
