## This is the main Apache server configuration file.  It contains the
## configuration directives that give the server its instructions.
## See http://httpd.apache.org/docs/2.4/ for detailed information about
## the directives.
##
## SPDX-License-Identifier: BSD-2-Clause
## SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>


## Global configuration
##

##
## ServerRoot: The top of the directory tree under which the server's
## configuration, error, and log files are kept.
##
## NOTE!  If you intend to place this on an NFS (or otherwise network)
## mounted filesystem then please read the Mutex documentation (available
## at <URL:http://httpd.apache.org/docs/2.4/mod/core.html##mutex>);
## you will save yourself a lot of trouble.
##
## Do NOT add a slash at the end of the directory path.
##
ServerName "@HOSTNAME@"
ServerRoot "@CMAKE_CURRENT_BINARY_DIR@/etc/apache2"
DocumentRoot "@CMAKE_CURRENT_BINARY_DIR@/var/www/html"

##
## The accept serialization lock file MUST BE STORED ON A LOCAL DISK.
##
##Mutex file:${APACHE_LOCK_DIR} default

##
## The directory where shm and other runtime files will be stored.
##

DefaultRuntimeDir "@CMAKE_CURRENT_BINARY_DIR@/var/run/apache2"

##
## PidFile: The file in which the server should record its process
## identification number when it starts.
##
PidFile "@CMAKE_CURRENT_BINARY_DIR@/var/run/apache2/apache2.pid"

##
## Timeout: The number of seconds before receives and sends time out.
##
Timeout 300

##
## KeepAlive: Whether or not to allow persistent connections (more than
## one request per connection). Set to "Off" to deactivate.
##
KeepAlive On

##
## MaxKeepAliveRequests: The maximum number of requests to allow
## during a persistent connection. Set to 0 to allow an unlimited amount.
## We recommend you leave this number high, for maximum performance.
##
MaxKeepAliveRequests 100

##
## KeepAliveTimeout: Number of seconds to wait for the next request from the
## same client on the same connection.
##
KeepAliveTimeout 5

##
## HostnameLookups: Log the names of clients or just their IP addresses
## e.g., www.apache.org (on) or 204.62.129.132 (off).
## The default is off because it'd be overall better for the net if people
## had to knowingly turn this feature on, since enabling it means that
## each client request will result in AT LEAST one lookup request to the
## nameserver.
##
HostnameLookups Off

## ErrorLog: The location of the error log file.
## If you do not specify an ErrorLog directive within a <VirtualHost>
## container, error messages relating to that virtual host will be
## logged here.  If you *do* define an error logfile for a <VirtualHost>
## container, that host's errors will be logged there and not here.
##
ErrorLog "@CMAKE_CURRENT_BINARY_DIR@/var/log/apache2/error.log"

##
## LogLevel: Control the severity of messages logged to the error_log.
## Available values: trace8, ..., trace1, debug, info, notice, warn,
## error, crit, alert, emerg.
## It is also possible to configure the log level for particular modules, e.g.
## "LogLevel info ssl:warn"
##
LogLevel warn

## Sets the default security model of the Apache2 HTTPD server. It does
## not allow access to the root filesystem outside of /usr/share and /var/www.
## The former is used by web applications packaged in Debian,
## the latter may be used for local directories served by the web server. If
## your system is serving content from a sub-directory in /srv you must allow
## access here, or in any related virtual host.
<Directory @CMAKE_CURRENT_BINARY_DIR@>
	Options FollowSymLinks
	AllowOverride None
	Require all denied
</Directory>

<Directory @CMAKE_CURRENT_BINARY_DIR@/var/www/>
	Options Indexes FollowSymLinks
	AllowOverride None
	Require all granted
</Directory>

## AccessFileName: The name of the file to look for in each directory
## for additional configuration directives.  See also the AllowOverride
## directive.
##
AccessFileName .htaccess

##
## The following lines prevent .htaccess and .htpasswd files from being
## viewed by Web clients.
##
<FilesMatch "^\.ht">
	Require all denied
</FilesMatch>


## Load standard apache modules
Include modules.conf
LoadModule include_module               @CMAKE_CURRENT_BINARY_DIR@@APACHE_MODULE_DIR@/mod_include.so
LoadModule mime_module                  @CMAKE_CURRENT_BINARY_DIR@@APACHE_MODULE_DIR@/mod_mime.so

## Load ARPA2 modules tested herein
LoadModule arpa2_pypeline_signal_module @CMAKE_BINARY_DIR@/mod_arpa2_pypeline_signal.so
##
<IfDefine APACHE2_LOAD_BASIC>
	LoadModule auth_basic_module @CMAKE_CURRENT_BINARY_DIR@@APACHE_MODULE_DIR@/mod_auth_basic.so
	LoadModule authn_file_module @CMAKE_CURRENT_BINARY_DIR@@APACHE_MODULE_DIR@/mod_authn_file.so
</IfDefine>
##
<IfDefine APACHE2_LOAD_SASL>
	LoadModule arpa2_sasl_module            @CMAKE_BINARY_DIR@/mod_arpa2_sasl.so
</IfDefine>
##
<IfDefine APACHE2_LOAD_DIASASL>
	LoadModule arpa2_diasasl_module         @CMAKE_BINARY_DIR@/mod_arpa2_diasasl.so
</IfDefine>
##
<IfDefine APACHE2_LOAD_USERDIR>
	LoadModule arpa2_userdir_module         @CMAKE_BINARY_DIR@/mod_arpa2_userdir.so
</IfDefine>
##
<IfDefine APACHE2_LOAD_ACLR>
	LoadModule arpa2_access_module         @CMAKE_BINARY_DIR@/mod_arpa2_access.so
</IfDefine>

<IfModule mod_mime.c>
        TypesConfig @CMAKE_CURRENT_SOURCE_DIR@/etc/mime.types
	DefaultLanguage en
</IfModule>

<IfModule mod_arpa2_sasl.c>
	<Directory "@CMAKE_CURRENT_BINARY_DIR@/var/www/html/diasasl">
		Options +Includes
		AuthType "SASL"
		AuthName "@http-realm@"
		SaslRealm "example.com"
		SaslMechanisms "DIGEST-MD5"
		SaslDbPath "@CMAKE_CURRENT_BINARY_DIR@@sasldb@"
		Require valid-user
		AddOutputFilter INCLUDES html
	</Directory>
	<Directory "@CMAKE_CURRENT_BINARY_DIR@/var/www/html/communication">
		Options +Includes
		AuthType "SASL"
		AuthName "@http-realm@"
		SaslRealm "example.com"
		SaslMechanisms "DIGEST-MD5"
		SaslDbPath "@CMAKE_CURRENT_BINARY_DIR@@sasldb@"
		Require valid-user
		AddOutputFilter INCLUDES html
	</Directory>
	<Directory "@CMAKE_CURRENT_BINARY_DIR@/var/www/html/documents">
		Options +Includes
		AuthType "SASL"
		AuthName "@http-realm@"
		SaslRealm "example.com"
		SaslMechanisms "DIGEST-MD5"
		SaslDbPath "@CMAKE_CURRENT_BINARY_DIR@@sasldb@"
		Require valid-user
		AddOutputFilter INCLUDES html
	</Directory>
	<Directory "@CMAKE_CURRENT_BINARY_DIR@/var/www/html/diasasl2">
		Options +Includes
		AuthType "SASL"
		AuthName "@http-pixie-realm@"
		SaslRealm "pixie.demo.arpa2.org"
		SaslMechanisms "DIGEST-MD5"
		SaslDbPath "@CMAKE_CURRENT_BINARY_DIR@@sasldb@"
		Require valid-user
		AddOutputFilter INCLUDES html
	</Directory>
</IfModule>

<IfModule mod_auth_basic.c>
	<Directory "@CMAKE_CURRENT_BINARY_DIR@/var/www">
		Options +Includes
		AuthType "basic"
		AuthName "@http-realm@"
		AuthUserFile "@CMAKE_CURRENT_BINARY_DIR@@basicdb@"
		Require valid-user
		AddOutputFilter INCLUDES html
	</Directory>
</IfModule>

<IfModule mod_arpa2_access.c>
	ServerRealm "example.com"
	## document service key for example.com domain
	DocumentServiceKey "832700dc78557ba8bdca2f0688773930b56d4db1a8c8af277e6c7063bee822bf"
	## communication service key for example.com domain
	CommunicationServiceKey "cfbe330a076a692e8f038c09d29abf9d8a103305c25e521d38c6a9d03311fb91"
	<DirectoryMatch "/var/www/html/documents/(?<dirname>[^/]+)">
		DocumentAccessName //Documents/%{env:MATCH_DIRNAME}
	</DirectoryMatch>
	<DirectoryMatch "/var/www/html/communication">
		CommunicationAccessName "mary@example.com"
	</DirectoryMatch>
</IfModule>

<IfModule mod_arpa2_diasasl.c>
	<Directory "@CMAKE_CURRENT_BINARY_DIR@/var/www/html/diasasl">
		Options +Includes
		AuthType "SASL"
		AuthName "@http-realm@"
		DiasaslIP #IP:DIASASL#
		DiasaslPort #TCP:DIASASL#
		DiasaslRealm "@sasldb-realm@"
		Require valid-user
		AddOutputFilter INCLUDES html
	</Directory>
	<Directory "@CMAKE_CURRENT_BINARY_DIR@/var/www/html/diasasl2">
		Options +Includes
		AuthType "SASL"
		AuthName "@http-pixie-realm@"
		DiasaslIP #IP:DIASASL2#
		DiasaslPort #TCP:DIASASL2#
		DiasaslRealm "pixie.demo.arpa2.org"
		Require valid-user
		AddOutputFilter INCLUDES html
	</Directory>
</IfModule>

<IfModule mod_arpa2_userdir.c>
	UserDir disabled
	UserDir enabled john mary
	UserDir "@CMAKE_CURRENT_SOURCE_DIR@/home/*/public_html"
	<Directory "@CMAKE_CURRENT_SOURCE_DIR@/home/*/public_html">
		Options Indexes FollowSymLinks Includes
		DirectoryIndex index.html index.md index.txt
		## LocalUserMatch [a-zA-Z_][a-zA-Z0-9_]*
		AddOutputFilter INCLUDES md
		AllowOverride None
		Require all granted
	</Directory>
</IfModule>

<IfModule mod_ssl.c>
	SSLEngine on
	SSLCertificateFile @CMAKE_CURRENT_BINARY_DIR@/apache.cert.pem
	SSLCertificateKeyFile @CMAKE_CURRENT_BINARY_DIR@/apache.key.pem
</IfModule>
