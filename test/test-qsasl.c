/* SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <pcre.h>

#include <unistd.h>

#include <curl/curl.h>


#include <sasl/saslutil.h>
#include <arpa2/quick-der.h>
#include <arpa2/quick-sasl.h>

#define DEBUG
#define DEBUG_DETAIL
#include <arpa2/except.h>

/* OpenSSL headers */
#include <openssl/opensslv.h>
#if (OPENSSL_VERSION_NUMBER >= 0x10001000)
/* must be defined before including ssl.h */
#define OPENSSL_NO_SSL_INTERN
#endif
#if OPENSSL_VERSION_NUMBER >= 0x30000000
#include <openssl/core_names.h>
#endif
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/crypto.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/x509v3.h>
#include <openssl/x509_vfy.h>
#include <openssl/ocsp.h>

#include <arpa2/util/snoprintf.h>

#define RE_SASL_MECH "[A-Z0-9-_]{1,20}"
#define RE_MECHSTRING "\"(" RE_SASL_MECH "(?:[ ]" RE_SASL_MECH ")*)\""
#define RE_REALMSTRING "\"((?:[^\"\\\\]|\\\\.)*)\""
#define RE_BWS  "[ \\t]*"
#define RE_OWS  RE_BWS
#define RE_BASE64STRING  "\"([a-zA-Z0-9-._~+/]*=*)\""
#define RE_AUTH_PARAM \
    "(?:" \
        "([CcSs][2][CcSs])" RE_BWS "=" RE_BWS RE_BASE64STRING \
        "|" \
        "([Mm][Ee][Cc][Hh])" RE_BWS "=" RE_BWS RE_MECHSTRING \
        "|" \
        "([Rr][Ee][Aa][Ll][Mm])" RE_BWS "=" RE_BWS RE_REALMSTRING \
    ")"
#define RE_AUTH_SCHEME  "[Ss][Aa][Ss][Ll]"
#define RE_CREDENTIALS  RE_AUTH_SCHEME "(?:[ ]+(" RE_AUTH_PARAM "(?:" \
    RE_OWS "," RE_OWS RE_AUTH_PARAM ")+)?)"

#define OVECCOUNT 30    /* should be a multiple of 3 */

#define SAMPLE_SEC_BUF_SIZE (2048)

typedef struct {
    char *mechs;
    char *realm;
    char *s2s;
    char *s2c;
} http_sasl_t;

static int ovector[OVECCOUNT];

static pcre *re_credentials;
static pcre *re_auth_parm;

#define HTTP_SERVICE_NAME "RealmCrossOver"

static char *authname = NULL;
static char *password = NULL;

static void check_re(const pcre *re, const char *error, int erroffset)
{
    /* Compilation failed: print the error message and exit */
    if (re == NULL)
    {
        fprintf(stderr, "PCRE compilation failed at offset %d: %s\n", erroffset, error);
    }
}

static char *get_value(char *auth_line, int i) {
    int substring_length = ovector[2 * i + 1] - ovector[2 * i];
    const char *substring_start = auth_line + ovector[2 * i];
    char *value = malloc(substring_length + 1);
    memcpy(value, substring_start, substring_length);
    value[substring_length] = '\0';
    return value;
}

void parse_www_authenticate_header(char *auth_line, int auth_line_len, http_sasl_t* http_sasl)
{
    http_sasl->mechs = NULL;
    http_sasl->s2c = NULL;
    http_sasl->s2s = NULL;
    http_sasl->realm = NULL;

    int rc = pcre_exec(
        re_credentials,    /* the compiled pattern */
        NULL,              /* no extra data - we didn't study the pattern */
        auth_line,         /* the subject string */
        auth_line_len,     /* the length of the subject */
        0,                 /* start at offset 0 in the subject */
        0,                 /* default options */
        ovector,           /* output vector for substring information */
        OVECCOUNT          /* number of elements in the output vector */
    );
    if (rc >= 0) {
        int start_offset = 0;

        for (;;) {
            rc = pcre_exec(
                re_auth_parm,      /* the compiled pattern */
                NULL,              /* no extra data - we didn't study the pattern */
                auth_line,         /* the subject string */
                auth_line_len,     /* the length of the subject */
                start_offset,      /* start at offset 0 in the subject */
                0,                 /* default options */
                ovector,           /* output vector for substring information */
                OVECCOUNT          /* number of elements in the output vector */
            );
            if (rc > 2) {
                char *key = get_value(auth_line, rc - 2);
                char *value = get_value(auth_line, rc - 1);
                fprintf(stderr, "setting ('%s', '%s')\n", key, value);
                            if (strcmp(key, "mech") == 0) {
                                http_sasl->mechs = value;
                            } else if (strcmp(key, "s2c") == 0) {
                                http_sasl->s2c = value;
                            } else if (strcmp(key, "s2s") == 0) {
                                http_sasl->s2s = value;
                            } else if (strcmp(key, "realm") == 0) {
                                http_sasl->realm = value;
                            }
                start_offset = ovector[1];
            } else {
                fprintf(stderr, "no (more) auth param matches\n");
                break;
            }
        }
    } else {
        printf("no credentials match\n");
    }
}

#define WWW_AUTHENTICATE "WWW-Authenticate: "
#define WWW_AUTHENTICATE_LEN 18
#define WWW_AUTHENTICATE_SIZE 4096

char www_authenticate[WWW_AUTHENTICATE_SIZE];
int  www_authenticate_len;

size_t header_callback(char *buffer, size_t size, size_t nitems, void *userdata) {
    int len = size * nitems;

    if (len >= WWW_AUTHENTICATE_LEN && len < WWW_AUTHENTICATE_SIZE - WWW_AUTHENTICATE_LEN && memcmp(buffer, WWW_AUTHENTICATE, WWW_AUTHENTICATE_LEN) == 0 ) {
        www_authenticate_len = len - WWW_AUTHENTICATE_LEN;
        memcpy(www_authenticate, buffer + WWW_AUTHENTICATE_LEN, www_authenticate_len);
    }
    return len;
}

/* SSLv3 uses 36 bytes for Finishd messages, TLS1.0 12 bytes,
 * So tls-unique is max 36 bytes, however with tls-server-end-point,
 * the CB data is the certificate signature, so we use the maximum
 * hash size known to the library (currently 64).
 * */
#define TLS_CB_MAX EVP_MAX_MD_SIZE
#define TLS_UNIQUE_PREFIX "tls-unique:"
#define TLS_SERVER_END_POINT_PREFIX "tls-server-end-point:"

#define strcEQ(s1,s2)    (strcasecmp(s1,s2) == 0)

static int my_ssl_get_tls_cb(SSL* ssl, const char *type, unsigned char **buf, size_t *size)
{
    const char *prefix;
    size_t preflen;
    const unsigned char *data;
    unsigned char cb[TLS_CB_MAX], *retbuf;
    unsigned int l = 0;
    X509 *x = NULL;

    if (!ssl) {
        return -1;
    }
    if (strcEQ(type, "CLIENT_TLS_UNIQUE")) {
        l = SSL_get_peer_finished(ssl, cb, TLS_CB_MAX);
    }
    else if (strcEQ(type, "SERVER_TLS_UNIQUE")) {
        l = SSL_get_finished(ssl, cb, TLS_CB_MAX);
    }
    else if (strcEQ(type, "CLIENT_TLS_SERVER_END_POINT")) {
        x = SSL_get_certificate(ssl);
    }
    else if (strcEQ(type, "SERVER_TLS_SERVER_END_POINT")) {
        x = SSL_get_peer_certificate(ssl);
    }
    if (l > 0) {
        preflen = sizeof(TLS_UNIQUE_PREFIX) -1;
        prefix = TLS_UNIQUE_PREFIX;
        data = cb;
    }
    else if (x != NULL) {
        const EVP_MD *md;
        md = EVP_get_digestbynid(X509_get_signature_nid(x));
        /* Override digest as specified by RFC 5929 section 4.1. */
        if (md == NULL || md == EVP_md5() || md == EVP_sha1()) {
            md = EVP_sha256();
        }
        if (!X509_digest(x, md, cb, &l)) {
            return -1;
        }

        preflen = sizeof(TLS_SERVER_END_POINT_PREFIX) - 1;
        prefix = TLS_SERVER_END_POINT_PREFIX;
        data = cb;
    }
    else {
        return -1;
    }

    retbuf = malloc(preflen + l);
    memcpy(retbuf, prefix, preflen);
    memcpy(&retbuf[preflen], data, l);
    *size = preflen + l;
    *buf = retbuf;

    return 0;
}

typedef struct {
    CURL *curl;
    bool do_ssl;
    unsigned char *buf;
    size_t size;
} wf_userdata_t;

static size_t wf(void *ptr, size_t size, size_t nmemb, wf_userdata_t *userdata)
{
    if (userdata->do_ssl) {
        const struct curl_tlssessioninfo *info = NULL;
        CURLcode res = curl_easy_getinfo(userdata->curl, CURLINFO_TLS_SSL_PTR, &info);
        if (info && !res) {
            if (CURLSSLBACKEND_OPENSSL == info->backend) {
                SSL *ssl = (SSL*) info->internals;
                log_debug("OpenSSL ver. %s\n", SSL_get_version(ssl));
                userdata->buf = NULL;
                userdata->size = 0;
                if (my_ssl_get_tls_cb(ssl, "SERVER_TLS_SERVER_END_POINT", &userdata->buf, &userdata->size) != 0) {
                    log_debug("error calling ssl_get_tls_cb");
                }
            }
        }
    }
    return fwrite(ptr, size, nmemb, stdout);
}


/* Main program.
 */
int main (int argc, char *argv []) {
    char *url;
    int rc = -1;
    char *mechanism = NULL;
    int testrun_count = 1;
    CURL *curl;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s http://user:password@host/path [number_of_runs] [mechanism]\n", argv[0]);
        return 1;
    }
    url = argv[1];
    if (argc > 2) {
        testrun_count = atoi(argv[2]);
    }
    if (argc > 3) {
        mechanism = argv[3];
    }
    printf ("--\n");
    fflush (stdout);
    curl = curl_easy_init();
    if (curl) {
        CURLcode res;
        const char *error;
        int erroffset;

        CURLU *urlu = curl_url();
        if (urlu) {
            CURLUcode ucode = curl_url_set(urlu, CURLUPART_URL, url, 0);
            if (!ucode) {
                ucode = curl_url_get(urlu, CURLUPART_USER, &authname, 0);
                if (!ucode) {
                    printf("the user is %s\n", authname);
                }
                ucode = curl_url_get(urlu, CURLUPART_PASSWORD, &password, 0);
                if (!ucode) {
                    printf("the password is %s\n", password);
                }
            }
            curl_url_cleanup(urlu);
        }
        re_credentials = pcre_compile(
            RE_CREDENTIALS, /* the pattern */
            0,              /* default options */
            &error,         /* for error message */
            &erroffset,     /* for error offset */
            NULL            /* use default character tables */
        );
        check_re(re_credentials, error, erroffset);
        re_auth_parm = pcre_compile(
            RE_AUTH_PARAM,  /* the pattern */
            0,              /* default options */
            &error,         /* for error message */
            &erroffset,     /* for error offset */
            NULL            /* use default character tables */
        );
        check_re(re_auth_parm, error, erroffset);
        //
        // Start the SASL session as a client.
        qsasl_init (NULL, "qsasl-xsasl-demo");
        for (int testrun = 0; testrun < testrun_count; testrun++) {
            curl_easy_setopt(curl, CURLOPT_URL, url);
            /* remove Authorization header */
            struct curl_slist *chunk = curl_slist_append(NULL, "Authorization:");
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
            curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_callback);
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, wf);
            wf_userdata_t userdata = {
                curl,
                false
            };
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &userdata);
            /* ask libcurl to show us the verbose output */
            curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
            char *cainfo = getenv("CURL_CA_BUNDLE");
            if (cainfo) {
                curl_easy_setopt(curl, CURLOPT_CAINFO, cainfo);
            }
            www_authenticate_len = 0;
            res = curl_easy_perform(curl);
            /* free Authorization header */
            curl_slist_free_all(chunk);
            /* Check for errors */
            if (res != CURLE_OK) {
                fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
            } else {
    //          printf("WWW-Authenticate: %.*s\n", www_authenticate_len, www_authenticate);
                if (www_authenticate_len == 0) {
                    printf("No WWW-Authenticate header\n");
                } else {
                    http_sasl_t http_sasl;

                    parse_www_authenticate_header(www_authenticate, www_authenticate_len, &http_sasl);
                    if (http_sasl.mechs != NULL) {
                        printf("mechanisms: %s\n", http_sasl.mechs);

                        QuickSASL sasl = NULL;
                        qsaslt_state curstate = QSA_UNDECIDED;
                        char *service = getenv("HTTP_SERVICE_NAME");
                        if (!service) {
                            service = HTTP_SERVICE_NAME;
                        }
                        fprintf(stderr, "HTTP service name: %s\n", service);
                        assert (qsasl_client (&sasl, service, NULL, &curstate, 0));
                        //
                        // Be someone -- including step down to the ACL user
                        //char *str_realm = getenv ("SASL_CLIENT_REALM"    );
                        //char *str_servr = getenv ("KIP_REALM"            );
                        //membuf crs_realm = { .bufptr=str_realm, .buflen=strlen (str_realm) };
                        if (http_sasl.realm != NULL) {
                            membuf crs_servr = {
                                .bufptr = http_sasl.realm,
                                .buflen = strlen(http_sasl.realm)
                            };
                            assert (qsasl_set_server_realm (sasl, crs_servr));
                            free(http_sasl.realm);
                        }
                        membuf crs_login = { .bufptr=authname, .buflen=strlen (authname) };
                        assert (qsasl_set_clientuser_login (sasl, crs_login));
                        //assert (qsasl_set_clientuser_acl   (sasl, crs_acl  ));
                        //assert (qsasl_set_client_realm     (sasl, crs_realm));
                        setenv("QUICKSASL_PASSPHRASE", password, 0);

                        membuf mechs;
                        if (mechanism == NULL) {
                            mechs.bufptr = http_sasl.mechs;
                            mechs.buflen = strlen(http_sasl.mechs);
                        } else {
                            mechs.bufptr = mechanism;
                            mechs.buflen = strlen(mechanism);
                        }
                        assert (qsasl_set_mech (sasl, mechs));
                        free(http_sasl.mechs);
                        //
                        // Start looping as a client
                        membuf s2c = {
                            .bufptr = NULL,
                            .buflen = 0
                        };
                        membuf mech_used = {
                            .bufptr = NULL,
                            .buflen = 0
                        };
                        while (curstate == QSA_UNDECIDED) {
                            //
                            // Make a client step: s2c --> mech, c2s
                            membuf c2s;
                            membuf mech;
                            assert (qsasl_step_client (sasl, s2c, &mech, &c2s));
                            bool success;
                            bool failure;
                            assert (qsasl_get_outcome(sasl, &success, &failure));
                            fprintf(stderr, "qsasl_get_outcome: succes = %d, failure = %d\n", success, failure);
                            if (success || failure) {
                                rc = !success;
                                break;
                            }

                            int offset = 0;
                            char *comma = "";
                            char authorization[4096];
                            char s2c_buf[1024];
                            offset = snoprintf(authorization, sizeof(authorization), offset, "Authorization: SASL ");
                            if (mech.bufptr != NULL) {
                                offset = snoprintf(
                                    authorization,
                                    sizeof(authorization),
                                    offset,
                                    "%smech=\"%.*s\"",
                                    comma,
                                    mech.buflen,
                                    mech.bufptr
                                );
                                comma = ",";
                                userdata.do_ssl = true;
                            } else {
                                userdata.do_ssl = false;
                            }
                            if (http_sasl.s2s != NULL) {
                                offset = snoprintf(
                                    authorization,
                                    sizeof(authorization),
                                    offset,
                                    "%ss2s=\"%s\"",
                                    comma,
                                    http_sasl.s2s
                                );
                                comma = ",";
                            }
                            if (c2s.bufptr != NULL) {
                                char buf[SAMPLE_SEC_BUF_SIZE];
                                unsigned int len;
                                int result = sasl_encode64(c2s.bufptr, c2s.buflen, buf, SAMPLE_SEC_BUF_SIZE, &len);
                                if (result == SASL_OK) {
                                    offset = snoprintf(
                                        authorization,
                                        sizeof(authorization),
                                        offset,
                                        "%sc2s=\"%.*s\"",
                                        comma,
                                        len,
                                        buf
                                    );
                                    comma = ",";
                                }
                            }
                            //
                            // Send c2s to the server
                            /* Add an Authorization header */
                            printf("\n\n%s\n", authorization);
                            chunk = curl_slist_append(NULL, authorization);
                            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
                            www_authenticate_len = 0;
                            res = curl_easy_perform(curl);
                            /* free Authorization custom header */
                            curl_slist_free_all(chunk);
                            if (res != CURLE_OK) {
                                fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
                            } else {
                                if (userdata.do_ssl) {
                                    log_data("ssl_get_tls_cb", userdata.buf, userdata.size, 0);
                                    free(userdata.buf);
                                }
                                long response_code;
                                curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
                                if (www_authenticate_len == 0) {
                                    printf("No WWW-Authenticate header\n");
                                    rc = response_code != 200L;
                                    break;
                                } else {
                                    parse_www_authenticate_header(www_authenticate, www_authenticate_len, &http_sasl);
                                    free(http_sasl.mechs);
                                    free(http_sasl.realm);
                                    //free(http_sasl.s2s);
                                    printf("s2c base64: %s\n", http_sasl.s2c);
                                    unsigned int len;
                                    int result = sasl_decode64(http_sasl.s2c, strlen(http_sasl.s2c), s2c_buf, sizeof(s2c_buf), &len);
                                    free(http_sasl.s2c);
                                    if (result == SASL_OK) {
                                        s2c.bufptr = s2c_buf;
                                        s2c.buflen = len;
                                    } else {
                                        printf("error in sasl_decode64\n");
                                        break;
                                    }
                                }
                            }
                            if (curstate != QSA_UNDECIDED) {
                                break;
                            }
                        }
                        //
                        // Cleanup & Closedown
                        qsasl_close (&sasl);
                    }
                }
            }
        }
        qsasl_fini ();
    }
    return rc;
}
