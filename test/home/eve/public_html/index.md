<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
-->

# Hello World

> *This is Eve's personal domain.  Enjoy!*

EXPECTED_USER=eve
REMOTE_USER=<!--#echo var="REMOTE_USER" -->
LOCAL_USER=<!--#echo var="LOCAL_USER" -->
REQUEST_URI=<!--#echo var="REQUEST_URI" -->
