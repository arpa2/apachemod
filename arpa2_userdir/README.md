<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
-->
# ARPA2 extension for UserDir

> *This module `mod_arpa2_userdir` is a drop-in replacement for `mod_userdir`
> that adds behaviour for the `User:` header and `LOCAL_USER` environment variable.*

**Purpose.**
With `mod_userdir`, a special pattern `/~username` in the beginning of a path can be interpreted, and used to redirect traffic to a user-specific portion of a webserver.  This has not been standardised and so it is impossible for tools to be certain that they are dealing with a user.

**Header.**
To allow clients to express the desire to contact a particular user under a site or domain, [draft-vanrein-httpauth-sasl](https://datatracker.ietf.org/doc/draft-vanrein-httpauth-sasl/) introduces a `User:` header that works mostly like the `Host:` header, except that it asks for a user name on the web server, rather than a host name.

Note that this concerns the user on the server side; authentication handles a user on the client side, and these happen to be the same when the client is accessing their own online account, but there are more cases, for which HTTP does not provide an answer.

**URIs.**
This plugin module makes no assumptions about URI formatting.  If a user name is present in a HTTP URI then it would be in line with the [authoritative userinfo in a URL](https://tools.ietf.org/html/rfc3986#section-3.2.1) that it is the server-side user, but the HTTP specification has thus far not adopted user names in its URL format.  There is a deprecated usage pattern of Basic authentication abusing this spot for client user names, but that is not in conflict with the specification and has never been standardised.

## Extra Facilities

**Facilities.**
In comparison to `mod_userdir`, the extra facilities in `mod_arpa2_userdir` are:

  * Recognise the `User:` header, remove percent encoding and constrain the permissible grammar
  * Return the `Vary: User` header to inform caches that the content returned depends on this header
  * Setup the `LOCAL_USER` environment variable for use in scripts

**Directives.**
The `UserDir` directive continues to work as before.  The module can be stopped from redirection to a user directory with `UserDir disable`, but the interpretation of the `User:` header and setup of the `LOCAL_USER` variable will always work because they do not force anything on the content.

The `LocalUserMatch` directive provides a regular expression that must be matched.  This is applied after percent decoding of the value in the `User:` header.  The `LOCAL_USER` variable is only set when this regular expression matches.  The pattern does have a default pattern, namely a sequence of at least 1 alphanumeric characters, the special characters `-`, `.` and `_`.  Internationalisation may add locally meaningful UTF-8 characters to this set.

**Internationalisation.**
Subject to compiled-in support in PCRE and Apache, it is possible to specify `LocalUserMatch` expressions as general as valid UTF-8 strings.  This is not currently the case for the default pattern, but should be considered by any script writer to be a possible future development which PCRE and Apache are compiled with this support.

**Choices.**
A few topics were open for some debate, and practical choices were made thusly:

  * When both a `User:` header and the `/~` form are supplied, the latter is skipped.  This allows a transition period during which clients derive the `User:` header from any source and apply it, while servers may still announce this form.  When `/~` is not used, it is also not skipped.  It is only skipped because `UserDir` is loaded.
  * The `User:` header can take many forms due to percent encoding.  The `LOCAL_USER` variable is set to the decoded form, which counts as the canonical form.  This is forwarded to the `UserDir` logic, provided that it does not contain `/` which could be a security problem.  Other characters will be dealt with by the file system (or not).
  * The default regular expression in `LocalUserMatch` is conservative, and ASCII-based.  Internationalisation MAY however modify that on future versions of this module.  Since it is not in the range of control of scripts, these SHOULD be resistant to values in `LOCAL_USER` that hold valid UTF-8 strings, subject to administrative policies.  Script writers MUST be mindful that administrative policies MAY change silently as a result of recompilation or a distribution package upgrade.


## Considerations

Full-blown user identities are not like `john` but like `john@example.com` -- they include a domain.
This is not current practice in web services, but it ought to be.  It would allow domain hosting
without changes to host names.  This can work towards the domain-bound records which, much like
SRV records for XMPP and SIP, could assign hosts to implement (parts of) the web namespace for a
domain.

This is part of a three-fold emancipation that could improve the web user experience dramatically:

  * Being able to host websites, or portions of websites, to various vendors running it at their
    own host names
  * Being able to allow the domain in the local user, where it can even work with different
    backends for the various domain names
  * Generally decoupling user names from web servers, as part of the removal of credentials that
    are specific for a single web server and treating integration of accounts as an exception --
    causing users to need as many accounts as they visit websites

An approach to do this could be an extra configuration directive that has no defaults and so
would only impact the logic in contexts that specify it:

```
LocalUserDomain example.com example.org example.net
```

This would state that

  * When the `User:` header mentions no `@` and so no domain, attach `@example.com` to it
  * When the `User:` header mentions a `@` then require anything after the first `@` to match one of these strings

A special form could initially set something that is not a domain because it lacks a dot, to indicate
that a domain is not appended by default and so it is required in the `User:` header,

```
LocalUserDomain sent example.com example.org example.net
```

The use of a domain may further expand the UserDir behaviour, inasfar as it is not disabled.
When `LocalUserDomain` is present, the path would not see `bob` inserted, but `example.com/bob`,
so the domain is inserted in the path, and *only then* the user.

