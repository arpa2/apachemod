# Module to find Apache APXS (developer tooling for Apache modules)

#
#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

# Tries to find apxs, and sets the following variables:
#
#   - Apxs_FOUND
#   - Apxs_EXECUTABLE
#   - APACHE_MODULE_DIR

include(FeatureSummary)
include(FindPackageHandleStandardArgs)

set(_description "Apache apxs (developer tool)")

set_package_properties(Apxs PROPERTIES
    DESCRIPTION "${_description}"
    URL "https://httpd.apache.org/docs/2.4/programs/apxs.html"
)

find_program(_APXS
    NAMES apxs
    DOC "${_description}"
    )

if(_APXS)
    execute_process(
        COMMAND ${_APXS} -q LIBEXECDIR
        OUTPUT_VARIABLE _APXS_LIBEXECDIR
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    set(APACHE_MODULE_DIR "${_APXS_LIBEXECDIR}" CACHE PATH "Installation directory for Apache modules")
    set(Apxs_EXECUTABLE "${_APXS}")
else()
    set(APACHE_MODULE_DIR "NOTFOUND")
endif()

find_package_handle_standard_args(
    Apxs
    DEFAULT_MSG
    Apxs_EXECUTABLE
    APACHE_MODULE_DIR
)
