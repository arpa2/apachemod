# Module to find PCRE (Perl Compatible Regular Expressions)

#
#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

# Finds PCRE and defines the following imported target:
#   pcre::pcre Perl Compatible Regular Expression library
#
# Defines the following variables for legacy use:
#   - pcre_FOUND
#   - PCRE_LIBRARY
#   - PCRE_INCLUDE_DIR

include(FeatureSummary)
include(FindPackageHandleStandardArgs)

set(_description "Perl Compatible Regular Expressions")

set_package_properties(pcre PROPERTIES
    DESCRIPTION "${_description}"
    URL "http://www.pcre.org/"
)

if(NOT WIN32)
    find_package(PkgConfig)
    pkg_check_modules(_pc libpcre)
endif()

find_path(PCRE_INCLUDE_DIR
    NAMES pcre.h
    HINTS ${_pc_INCLUDE_DIRS}
)

if(PCRE_INCLUDE_DIR)
    find_library(PCRE_LIBRARY
        NAMES pcre libpcre
        HINTS ${_pc_LIBRARY_DIRS}
        PATHS ${_pc_ROOT}/lib
    )
endif()

find_package_handle_standard_args(pcre DEFAULT_MSG PCRE_LIBRARY PCRE_INCLUDE_DIR)

if(pcre_FOUND)
    add_library(pcre::pcre UNKNOWN IMPORTED)
    set_target_properties(pcre::pcre PROPERTIES
        IMPORTED_LOCATION ${PCRE_LIBRARY}
        INTERFACE_INCLUDE_DIRECTORIES ${PCRE_INCLUDE_DIR}
    )
endif()
