# Convenience macros for building an Apache module

#
#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

# The macro `add_apache_module` builds an Apache module using apxs,
# while preserving source-integrity (by copying the sources to
# the build-dir) and linking extra libraries.
#
# add_apache_module(
#   NAME <name>
#   SOURCES <file ...>
#   [LIBRARIES <lib ...>]
#   [EXTRA_CFLAGS <flag ...>]
# )
#
# Builds a module named <name> from the <file> sources. Optionally,
# link in the libraries <lib> and pass the compile flags <flag>
# to the build.
#
# Libraries given in <lib> may be IMPORTED targets, in which case
# the library file and includes are added to the build.
#
#
# There is no useful target that comes out of this macro, since it
# uses add_custom_command() for the actual build; you can't use the
# target_*() CMake commands on it afterwards.

find_package(Apxs)

function(add_apache_module)
    # No flags
    set(onevalued NAME)
    set(multivalued EXTRA_CFLAGS SOURCES LIBRARIES)

    cmake_parse_arguments(_aam "" "${onevalued}" "${multivalued}" ${ARGN})

    if(NOT _aam_NAME)
        message(FATAL_ERROR "Missing NAME in add_apache_module()")
    endif()

    if(NOT _aam_SOURCES)
        message(FATAL_ERROR "No SOURCES given to add_apache_module()")
    endif()

    if(NOT Apxs_EXECUTABLE)
        message(FATAL_ERROR "No apxs is available for add_apache_module()")
    endif()

    # Glue together the libraries as -Wl flags for libtool; this
    # might be shorter with list(JOIN ..) but we'd need to prepend
    # one more -Wl to make it work.
    #
    # Take care that _aam_LIB_FLAGS is a LIST, so use it unquoted.
    set(_aam_LIB_FLAGS "")
    if(_aam_LIBRARIES)
        foreach(_l ${_aam_LIBRARIES})
            # Get name of library. Fight libtool, which ignores full-path-to
            # library when passed as as linker option; split it into -l and -L
            # flags.
            set(_tf "${_l}")
            if(TARGET ${_l})
                get_target_property(_tf ${_l} LOCATION)
                if(NOT _tf)
                    set(_tf "${_l}")
                endif()
            endif()
            if (IS_ABSOLUTE "${_tf}")
                message(STATUS "Adding target ${_l} link flags to ${_aam_NAME}")
                get_filename_component(_tf_dir "${_tf}" DIRECTORY)
                get_filename_component(_tf_lib "${_tf}" NAME_WE)
                if(_tf_lib MATCHES "^lib")
                    string(SUBSTRING "${_tf_lib}" 3 -1 _tf_lib)
                    list(APPEND _aam_LIB_FLAGS "-l" "${_tf_lib}" "-Wl,-L${_tf_dir}")
                else()
                    list(APPEND _aam "-l" "${_tf}")
                endif()
            else()
                list(APPEND _aam_LIB_FLAGS "-l" "${_tf}")
            endif()
        endforeach()
    endif()

    # Glue together the C flags as -Wc flags for apxs / libtool.
    set(_aam_CFLAGS "")
    if(_aam_EXTRA_CFLAGS)
        foreach(_l ${_aam_EXTRA_CFLAGS})
            list(APPEND _aam_CFLAGS "-Wc,${_l}")
        endforeach()
    endif()

    # Glue together include-directories, for imported targets
    set(_aam_INCLUDES "")
    if(_aam_LIBRARIES)
        foreach(_l ${_aam_LIBRARIES})
            if(TARGET ${_l})
                get_target_property(_incd ${_l} INCLUDE_DIRECTORIES)
                get_target_property(_inci ${_l} INTERFACE_INCLUDE_DIRECTORIES)
                foreach(_d ${_incd} ${_inci})
                    if (_d)
                        message(STATUS "Adding target ${_l} include directory ${_d} to ${_aam_NAME}")
                        list(APPEND _aam_INCLUDES "-I;${_d}")
                    endif()
                endforeach()
            endif()
        endforeach()
    endif()

    # This is the important bit to create
    set(_soname "mod_${_aam_NAME}.so")
    # This is what apxs / libtool will **actually** create before install
    set(_laname "mod_${_aam_NAME}.la")
    set(_installdir "${CMAKE_INSTALL_PREFIX}${APACHE_MODULE_DIR}")
    set(_libexec "LIBEXECDIR=${_installdir}")

    # This is a terrible hack, but libtool does not understand
    # the distinction between source and build directories; naming
    # source files means that build artifacts are created there.
    # So we copy the sources into the build dir.
    set(_copied_sources "")
    foreach(_s ${_aam_SOURCES})
        set(_source ${CMAKE_CURRENT_SOURCE_DIR}/${_s})
        set(_output ${CMAKE_CURRENT_BINARY_DIR}/${_s})
        add_custom_command(
            OUTPUT ${_output}
            MAIN_DEPENDENCY ${_source}
            COMMAND ${CMAKE_COMMAND} -E copy ${_source} ${_output}
        )
        list(APPEND _copied_sources ${_output})
    endforeach()

    # Build-time: do the compilation, with the eventual module-dir
    # included in the build; this is kind of flaky because it's using
    # the module directory (which is probably an absolute path tied
    # to the Apache installation) but then prefixing with the local
    # install prefix.
    add_custom_command(
        OUTPUT ${_laname}
        COMMAND ${Apxs_EXECUTABLE}
            -c
            -S ${_libexec}
            -o ${_soname}
            ${_aam_INCLUDES}
            ${_aam_CFLAGS}
            ${_aam_LIB_FLAGS}
            ${_copied_sources}
        COMMAND ${CMAKE_COMMAND} -E copy .libs/${_soname} .
        DEPENDS ${_copied_sources}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )
    add_custom_target(${_aam_NAME} ALL DEPENDS ${_laname})

    # `apxs -i` (or libtool) assumes that the directory exists, which
    # makes sense when using the default APACHE_MODULE_DIR -- but since
    # we bung CMAKE_INSTALL_PREFIX in front, that directory may **not**
    # exist yet. The other tools aren't smart enough to create the
    # directory for us.
    install(
        CODE
            "
            make_directory(${_installdir})
            execute_process(COMMAND ${Apxs_EXECUTABLE} -i -n ${_aam_NAME} -S ${_libexec} ${_laname})
            "
    )
endfunction()
