
<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
-->
# ARPA2 access module

> *This module `mod_arpa2_access` implements access control using ARPA2's [arpa2common](https://gitlab.com/arpa2/arpa2common/-/blob/master/README.MD) library.*

This module uses apache's [mod_authn_core](https://httpd.apache.org/docs/2.4/mod/mod_authn_core.html)  and [mod_authz_core](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html) modules. It can be used in combination with either [mod_auth_basic](http://httpd.apache.org/docs/2.4/mod/mod_auth_basic.html), [mod_arpa2_sasl](https://gitlab.com/arpa2/apachemod/-/blob/master/arpa2_sasl/README.md) or [mod_arpa2_diasasl](https://gitlab.com/arpa2/apachemod/-/blob/master/arpa2_diasasl/README.md)

**Directives.**
|Name|Scope|Description
|--|--|--
|`ServerRealm`|server|The domain under which this server runs.  Since HTTP does not service a domain but a host name, it is not generally possible to derive this from other configuration directives.  Domains form the scope for ARPA2 Access Control, as well as for administrative control.  The same domain may be used in a multitude of domains, and Access Control is likely to tap into a shared configuration for a domain, eg.`example.com`
|`DocumentServiceKey`|server|The hexadecimal form of an ARPA2 Service Key for Document Access.  This value is normally supplied by a domain administrator, and is specific for the hosted domain and the Document Access Type, and may have a hashed-in Database Secret. eg. `"832700dc78557ba8bdca2f0688773930b56d4db1a8c8af277e6c7063bee822bf"`
|`CommunicationServiceKey`|server|The hexadecimal form of an ARPA2 Service Key for Communication Access.  This value is normally supplied by a domain administrator, and is specific for the hosted domain and the Communication Access Type, and may have a hashed-in Database Secret. eg. `"cfbe330a076a692e8f038c09d29abf9d8a103305c25e521d38c6a9d03311fb91"`
|`DocumentAccessName`|directory|The form `//volume>/<path>` or, specifically for the ARPA2 Reservoir, the form `/<colluuid>/` where the `<volume>` and `<path>` are free forms and `<colluuid>` is a Collection UUID.  This is a regular expression form, so it may contain path elements.|see below
|`CommunicationAccessName`|directory|The `user@domain.name` identity to which the authenticated REMOTE_USER is hoping to connect.  This is a regular expression form, so it may contain path elements|sasa

When neither Access Name is specified, there will be no Access Control being enforced.


### DocumentAccessName example
```
<DirectoryMatch "/var/www/html/documents/(?<dirname>[^/]+)">
	DocumentAccessName //Documents/%env:MATCH_DIRNAME}
</DirectoryMatch>
```

### CommunicationAccessName example
```
<DirectoryMatch "/var/www/html/communication">
	CommunicationAccessName "mary@example.com"
</DirectoryMatch>
```
