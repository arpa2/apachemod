/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 */

#include "ap_config.h"
#include "ap_mmn.h"
#include "httpd.h"
#include "http_config.h"
#include "http_connection.h"
#include "http_core.h"
#include "http_log.h"
#include "http_vhost.h"
#include "http_request.h"
#include "apr_strings.h"

#include <arpa2/identity.h>
#include <arpa2/access_document.h>
#include <arpa2/access_comm.h>
#include <arpa2/com_err.h>

#include <com_err/arpa2access.h>

#define ACL_GET                       "R"
#define ACL_HEAD                      "P"
#define ACL_POST                      "C"
#define ACL_PUT                       "W"
#define ACL_DELETE                    "D"
#define ACL_PATCH                     "W"

#define HTTPUUID "7a35d76d-a754-35a6-abe7-757c161f0263"

module AP_MODULE_DECLARE_DATA arpa2_access_module;

#define CONTEXT_LENGTH 1024
#define SERVICEKEY_LENGTH 32
#define ACCESNAME_LENGTH 1024

typedef struct {
    char *server_realm;
    unsigned char documentservicekey[SERVICEKEY_LENGTH];
    unsigned char communicationservicekey[SERVICEKEY_LENGTH];
} access_server_config;

typedef struct {
    char context[CONTEXT_LENGTH];
    ap_expr_info_t *documentaccessname;
    ap_expr_info_t *communicationaccessname;
} access_dir_config;

typedef enum {
    cmd_server_realm,
    cmd_documentservicekey,
    cmd_communicationservicekey,
    cmd_documentaccessname,
    cmd_communicationaccessname
} cmd_parts;

static void trace_nocontext(apr_pool_t *p, const char *file, int line,
                            const char *note)
{
    /*
     * Since we have no request or connection to trace, or any idea
     * from where this routine was called, there's really not much we
     * can do.  If we are not logging everything by way of the
     * EXAMPLE_LOG_EACH constant, do nothing in this routine.
     */

    ap_log_perror(file, line, APLOG_MODULE_INDEX, APLOG_NOTICE, 0, p,
                  APLOGNO(03297) "%s", note);
}

/*
 * Server config for this module
 */

static void *create_access_server_config(apr_pool_t *p, server_rec *s)
{
    access_dir_config *sconf = apr_pcalloc(p, sizeof(*sconf));

    return sconf;
}

static void *create_access_dir_config(apr_pool_t *p, char *context)
{
    char *note;
    access_dir_config *dconf = apr_pcalloc(p, sizeof *dconf);
    if (dconf) {
        char *note = apr_psprintf(p, "create_access_dir_config(dconf == %pp, context == %s)",   (void*) dconf, context);
        trace_nocontext(p, __FILE__, __LINE__, note);
        if (context) {
            if (strlen(context) < CONTEXT_LENGTH - 1) {
                strcpy(dconf->context, context);
            } else {
                note = "context TOO LONG";
                trace_nocontext(p, __FILE__, __LINE__, note);
            }
        }
    } else {
        trace_nocontext(p, __FILE__, __LINE__, "ERROR creating directory configuration");
    }
    return dconf;
}

static access_server_config *our_sconfig(const server_rec *s)
{
    return (access_server_config *) ap_get_module_config(s->module_config, &arpa2_access_module);
}

/*
 * Locate our directory configuration record for the current request.
 */
static access_dir_config *our_dconfig(const request_rec *r)
{
    return (access_dir_config *) ap_get_module_config(r->per_dir_config, &arpa2_access_module);
}

static void hex2bin(const char *hexstring, unsigned char* out, size_t out_len)
{
    const char *pos = hexstring;
    char byte[3];

    byte[2] = '\0';
    for (size_t count = 0; count < out_len; count++) {
        byte[0] = *pos++;
        byte[1] = *pos++;
        apr_int64_t value = apr_strtoi64(byte, NULL, 16);
        if (errno == 0) {
            out[count] = (unsigned char) value;
        }
    }
}

static const char *access_param(cmd_parms *cmd, void *conf, const char *val)
{
    access_dir_config *dconf = (access_dir_config *) conf;
    access_server_config *sconf = our_sconfig(cmd->server);
    ssize_t updated;

    switch ((long) cmd->info) {
        case cmd_server_realm: {
            sconf->server_realm = apr_pstrdup(cmd->pool, val);
            char *note = apr_psprintf(cmd->pool, "access_param: sconf = %pp, ServerRealm = %s", sconf, val);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
            break;
        }
        case cmd_documentservicekey: {
            char *note = apr_psprintf(cmd->pool, "access_param: sconf = %pp, DocumentServiceKey = %s", sconf, val);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
            hex2bin(val, sconf->documentservicekey, SERVICEKEY_LENGTH);
            break;
        }
        case cmd_communicationservicekey: {
            char *note = apr_psprintf(cmd->pool, "access_param: sconf = %pp, CommunicationServiceKey = %s", sconf, val);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
            hex2bin(val, sconf->communicationservicekey, SERVICEKEY_LENGTH);
            break;
        }
        case cmd_documentaccessname: {
            if (dconf->communicationaccessname) {
                 trace_nocontext (cmd->pool, __FILE__, __LINE__, "Cannot add a DocumentAccessName on top of a CommunicationAccessName");
                 break;
            }
            char *note = apr_psprintf(cmd->pool, "access_param: dconf = %pp, DocumentAccessName = %s", dconf, val);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
            const char *expr_err = NULL;
            dconf->documentaccessname = ap_expr_parse_cmd(cmd, val, AP_EXPR_FLAG_STRING_RESULT, &expr_err, NULL);
            if (expr_err) {
                note = apr_pstrcat(cmd->pool, "Cannot parse expression '", val, "' in DocumentAccessName: ", expr_err, NULL);
                trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
            }
            break;
        }
        case cmd_communicationaccessname: {
            if (dconf->documentaccessname) {
                 trace_nocontext (cmd->pool, __FILE__, __LINE__, "Cannot add a CommunicationAccessName on top of a DocumentAccessName");
                 break;
            }
            char *note = apr_psprintf(cmd->pool, "access_param: dconf = %pp, CommunicationAccessName = %s", dconf, val);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
            const char *expr_err = NULL;
            dconf->communicationaccessname = ap_expr_parse_cmd(cmd, val, AP_EXPR_FLAG_STRING_RESULT, &expr_err, NULL);
            if (expr_err) {
                note = apr_pstrcat(cmd->pool, "Cannot parse expression '", val, "' in CommunicationAccessName: ", expr_err, NULL);
                trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
            }
            break;
        }
    }
    return NULL;
}

static const command_rec access_cmds[] = {
    AP_INIT_TAKE1("ServerRealm", access_param, (void*)cmd_server_realm, RSRC_CONF,
                  "Server Realm"),
    AP_INIT_TAKE1("DocumentServiceKey", access_param, (void*)cmd_documentservicekey, RSRC_CONF,
                  "Document Service Key"),
    AP_INIT_TAKE1("CommunicationServiceKey", access_param, (void*)cmd_communicationservicekey, RSRC_CONF,
                  "Communication Service Key"),
    AP_INIT_TAKE1("DocumentAccessName", access_param, (void*)cmd_documentaccessname, OR_AUTHCFG,
                  "Document Access Name"),
    AP_INIT_TAKE1("CommunicationAccessName", access_param, (void*)cmd_communicationaccessname, OR_AUTHCFG,
                  "Communication Local ID"),
    {NULL}
};

static void handle_actor(request_rec *r, a2act_t *actor)
{
    if (a2act_isempty(actor)) {
        apr_table_setn(r->subprocess_env, "REMOTE_MASQUERADE", "FALSE");
        trace_nocontext(r->pool, __FILE__, __LINE__, "No actor");
    } else {
        apr_table_setn(r->subprocess_env, "REMOTE_MASQUERADE", "TRUE");
        r->user = apr_pstrdup(r->pool, actor->txt);
        char *note = apr_psprintf(r->pool, "actor: %s", actor->txt);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
    }
}

static int handle_document(request_rec *r, a2id_t *remote, char *reqright)
{
    access_server_config *sconf = our_sconfig(r->server);
    access_dir_config *dconf = our_dconfig(r);
    access_rights exp_rights = 0;
    access_parse(reqright, &exp_rights);
    int rc = HTTP_INTERNAL_SERVER_ERROR;

    char *note = apr_psprintf(r->pool, "Match regex: %s", dconf->context);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);

    const char *err = NULL;
    const char *documentaccessname = ap_expr_str_exec(r, dconf->documentaccessname, &err);
    if (err) {
        note = apr_psprintf(r->pool, "DocumentAccessName expression could not be evaluated: %s", err);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
    } else {
        note = apr_psprintf(r->pool, "DocumentAccessName: %s", documentaccessname);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
        access_rights act_rights;
        a2act_t act_actor;
        if (access_document(remote, (char *) documentaccessname, sconf->documentservicekey, SERVICEKEY_LENGTH, NULL, 0, &act_rights, &act_actor)) {
            errno = 0;
            char act_rstr[27];
            access_format(NULL, act_rights, act_rstr);
            apr_table_setn(r->subprocess_env, "ACCESS_RIGHTS", apr_pstrdup(r->pool, act_rstr));
            note = apr_psprintf(r->pool, "act_rights: %s (0x%08x)", act_rstr, act_rights);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            if ((act_rights & exp_rights) == exp_rights) {
                note = apr_psprintf(r->pool, "%s: PERMITTED", reqright);
                trace_nocontext(r->pool, __FILE__, __LINE__, note);
                handle_actor(r, &act_actor);
                rc = OK;
            } else {
                note = apr_psprintf(r->pool, "%s: FORBIDDEN", reqright);
                trace_nocontext(r->pool, __FILE__, __LINE__, note);
                rc = HTTP_FORBIDDEN;
            }
        } else {
            trace_nocontext(r->pool, __FILE__, __LINE__, "access_document FAILS");
        }
    }
    return rc;
}

static char *access_level_to_string(access_comm_level level) {
    switch (level) {
    case access_comm_whitelist:
        return "access_comm_whitelist";
    case access_comm_greylist:
        return "access_comm_greylist";
    case access_comm_blacklist:
        return "access_comm_blacklist";
    case access_comm_honeypot:
        return "access_comm_honeypot";
    case access_comm_undefined:
        return "access_comm_undefined";
    default:
        return "UNKNOWN";
    }
}

static int handle_communication(request_rec *r, a2id_t *remote)
{
    access_server_config *sconf = our_sconfig(r->server);
    access_dir_config *dconf = our_dconfig(r);
    int rc = HTTP_INTERNAL_SERVER_ERROR;

    char *note = apr_psprintf(r->pool, "Match regex: %s", dconf->context);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);
    const char *err = NULL;
    const char *communicationaccessname = ap_expr_str_exec(r, dconf->communicationaccessname, &err);
    if (err) {
        note = apr_psprintf(r->pool, "CommunicationAccessName expression could not be evaluated: %s", err);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
    } else {
        note = apr_psprintf(r->pool, "CommunicationAccessName: %s", communicationaccessname);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
        a2id_t local;
        if (a2id_parse(&local, communicationaccessname, 0)) {
            access_comm_level act_level;
            a2act_t act_actor;
            if (access_comm(remote, &local, sconf->communicationservicekey, SERVICEKEY_LENGTH, NULL, 0, &act_level, &act_actor)) {
                errno = 0;
                char *act_level_str = access_level_to_string(act_level);
                apr_table_setn(r->subprocess_env, "ACCESS_LEVEL", apr_psprintf(r->pool, "%d", act_level));
                note = apr_psprintf(r->pool, "act_level: %s (%d)", act_level_str, act_level);
                trace_nocontext(r->pool, __FILE__, __LINE__, note);
                if (act_level == access_comm_whitelist) {
                    note = apr_psprintf(r->pool, "PERMITTED");
                    trace_nocontext(r->pool, __FILE__, __LINE__, note);
                    handle_actor(r, &act_actor);
                    rc = OK;
                } else {
                    note = apr_psprintf(r->pool, "FORBIDDEN");
                    trace_nocontext(r->pool, __FILE__, __LINE__, note);
                    rc = HTTP_FORBIDDEN;
                }
            } else {
                trace_nocontext(r->pool, __FILE__, __LINE__, "access_comm FAILS");
            }
        }
    }
    return rc;
}

/*
 * This routine is called to check to see if the resource being requested
 * requires authorisation.
 *
 * This is a RUN_FIRST hook. The return value is OK, DECLINED, or
 * HTTP_mumble.  If we return OK, no other modules are called during this
 * phase.
 *
 * If *all* modules return DECLINED, the request is aborted with a server
 * error.
 */
static int x_check_authz(request_rec *r)
{
    char *reqright;

    access_server_config *sconf = our_sconfig(r->server);
    access_dir_config *dconf = our_dconfig(r);
    int rc = HTTP_INTERNAL_SERVER_ERROR;
    errno = 0;

    const char *realm = ap_auth_name(r);
    char *remote_user = r->user;
    char *uri = r->uri;
    const char *server_realm = sconf->server_realm ? sconf->server_realm : r->hostname; 
    char *note = apr_psprintf(r->pool, "x_check_authz(): user = %s, server_realm = %s, uri = %s, realm = %s, ap_auth_type = %s, method = %s",
                    remote_user, server_realm, uri, realm, r->ap_auth_type, r->method);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);
    a2id_t remote;
    if (a2id_parse_remote(&remote, remote_user, 0)) {
        switch (r->method_number) {
        case M_GET:
            reqright = ACL_GET;
            break;
/* no M_HEAD?
        case M_HEAD:
            reqright = ACL_HEAD;
            break;
*/
        case M_POST:
            reqright = ACL_POST;
            break;
        case M_PUT:
            reqright = ACL_PUT;
            break;
        case M_DELETE:
            reqright = ACL_DELETE;
            break;
        }
        note = apr_psprintf(r->pool, "looking up %s, dconf: %pp", reqright, dconf);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
        if (dconf->documentaccessname) {
            rc = handle_document(r, &remote, reqright);
        } else if (dconf->communicationaccessname) {
            rc = handle_communication(r, &remote);
        } else {
            /* accept request due to absent Access Name, see issue #6 */
            rc = OK;
        }
    }
    if ((rc != OK) && errno) {
        char *com_err_hdr = apr_psprintf(r->pool, "%s (%d)", error_message(errno), errno);
        note = apr_psprintf(r->pool, "com_err: %s", com_err_hdr);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
        apr_table_setn(
            r->err_headers_out,
            "X-Common-Error",
            com_err_hdr
        );
    }
    return rc;
}

static void arpa2_access_fini(apr_pool_t *p)
{
    trace_nocontext(p, __FILE__, __LINE__, "arpa2_acces_fini");
    a2id_fini();
    access_fini();
}

static void arpa2_access_init(apr_pool_t *p, server_rec *s)
{
    trace_nocontext(p, __FILE__, __LINE__, "arpa2_acces_init");
    apr_pool_cleanup_register(p, p, (void*) arpa2_access_fini, apr_pool_cleanup_null);
    a2id_init();
    access_init();
}

static void register_hooks(apr_pool_t *p)
{
    ap_hook_child_init(arpa2_access_init, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_check_authz(x_check_authz, NULL, NULL, APR_HOOK_MIDDLE,
                        AP_AUTH_INTERNAL_PER_CONF);
}

AP_DECLARE_MODULE(arpa2_access) = {
    STANDARD20_MODULE_STUFF,
    create_access_dir_config,        /* create per-directory config structure */
    NULL,                          /* merge per-directory config structures */
    create_access_server_config,     /* create per-server config structure */
    NULL,                          /* merge per-server config structures */
    access_cmds,                     /* command apr_table_t */
    register_hooks                 /* register hooks */
};
