/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 */

#include <assert.h>
#include "ap_config.h"
#include "ap_mmn.h"
#include "httpd.h"
#include "http_config.h"
#include "http_connection.h"
#include "http_core.h"
#include "http_log.h"
#include "http_vhost.h"
#include "http_request.h"
#include "apr_strings.h"
#include "apr_base64.h"
#include "mod_ssl.h"

#include <arpa2/quick-der.h>
#include <arpa2/quick-dermem.h>
#include <arpa2/quick-diasasl.h>
#include <arpa2/xover-sasl.h>

#define DEBUG
#define DEBUG_DETAIL

#include <arpa2/except.h>
#include <arpa2/socket.h>
#include <com_err/Quick-DiaSASL.h>

/* OpenSSL headers */
#include <openssl/opensslv.h>
#if (OPENSSL_VERSION_NUMBER >= 0x10001000)
/* must be defined before including ssl.h */
#define OPENSSL_NO_SSL_INTERN
#endif
#if OPENSSL_VERSION_NUMBER >= 0x30000000
#include <openssl/core_names.h>
#endif
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/crypto.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/x509v3.h>
#include <openssl/x509_vfy.h>
#include <openssl/ocsp.h>

#include <pcre.h>
#include <pthread.h>

#define SAMPLE_SEC_BUF_SIZE (2048)


#define RE_SASL_MECH "[A-Z0-9-_]{1,20}"
#define RE_MECHSTRING "\"(" RE_SASL_MECH "(?:[ ]" RE_SASL_MECH ")*)\""
#define RE_REALMSTRING "\"((?:[^\"\\\\]|\\\\.)*)\""
#define RE_BWS  "[ \\t]*"
#define RE_OWS  RE_BWS
#define RE_BASE64STRING  "\"([a-zA-Z0-9-._~+/]*=*)\""
#define RE_AUTH_PARAM \
    "(?:" \
        "([CcSs][2][CcSs])" RE_BWS "=" RE_BWS RE_BASE64STRING \
        "|" \
        "([Mm][Ee][Cc][Hh])" RE_BWS "=" RE_BWS RE_MECHSTRING \
        "|" \
        "([Rr][Ee][Aa][Ll][Mm])" RE_BWS "=" RE_BWS RE_REALMSTRING \
    ")"
#define RE_AUTH_SCHEME  "[Ss][Aa][Ss][Ll]"
#define RE_CREDENTIALS  RE_AUTH_SCHEME "(?:[ ]+(" RE_AUTH_PARAM "(?:" \
    RE_OWS "," RE_OWS RE_AUTH_PARAM ")+)?)"

#define myConnConfig(c) (SSLConnRec *)ap_get_module_config(c->conn_config, ssl_module)
#define strcEQ(s1,s2)    (strcasecmp(s1,s2) == 0)

#define MAX_MECH_LEN 20

module AP_MODULE_DECLARE_DATA arpa2_diasasl_module;

struct diasasl_data {
    struct diasasl_session diasasl;
    pthread_mutex_t master_action;
    dercursor mechlist;
    dercursor s2c_token;
    bool success;
    bool failure;
    int com_errno;
    char mech[MAX_MECH_LEN + 1];
};

typedef struct {
    /* Server configuration as applicable */
    /* A persistent but variable resource */
    apr_pool_t *pool;
#if APR_HAS_THREADS
    apr_thread_mutex_t *mutex;
#endif
    apr_hash_t *connections;
    apr_hash_t *sessions;
    apr_array_header_t *diamaster;
} sasl_server_config;

typedef struct {
    /* configuration directives */
    char *diasasl_realm;
    char *diasasl_ip;
    char *diasasl_port;
    /* mechanism list */
    char mechlist[1024];
} sasl_dir_config;

typedef enum {
    cmd_diasasl_ip,
    cmd_diasasl_port,
    cmd_diasasl_realm
} cmd_parts;

typedef apr_status_t (*ssl_get_tls_cb_t)
(
    apr_pool_t    *p,
    conn_rec      *c,
    const char    *type,
    unsigned char **buf,
    apr_size_t    *size
);

typedef struct {
    SSL *ssl;
} SSLConnRec;

static ssl_get_tls_cb_t ssl_get_tls_cb;
static module *ssl_module;

static const SSLConnRec *ssl_get_effective_config(conn_rec *c)
{
    const SSLConnRec *sslconn = myConnConfig(c);
    if (!(sslconn && sslconn->ssl) && c->master) {
        /* use master connection if no SSL defined here */
        sslconn = myConnConfig(c->master);
    }
    return sslconn;
}

/* SSLv3 uses 36 bytes for Finishd messages, TLS1.0 12 bytes,
 * So tls-unique is max 36 bytes, however with tls-server-end-point,
 * the CB data is the certificate signature, so we use the maximum
 * hash size known to the library (currently 64).
 * */
#define TLS_CB_MAX EVP_MAX_MD_SIZE
#define TLS_UNIQUE_PREFIX "tls-unique:"
#define TLS_SERVER_END_POINT_PREFIX "tls-server-end-point:"

static apr_status_t my_ssl_get_tls_cb(apr_pool_t *p, conn_rec *c, const char *type,
                                   unsigned char **buf, apr_size_t *size)
{
    SSLConnRec *sslconn = myConnConfig(c);
    const char *prefix;
    apr_size_t preflen;
    const unsigned char *data;
    unsigned char cb[TLS_CB_MAX], *retbuf;
    unsigned int l = 0;
    X509 *x = NULL;

    if (!sslconn || !sslconn->ssl) {
        return APR_EGENERAL;
    }
    if (strcEQ(type, "SERVER_TLS_UNIQUE")) {
        l = SSL_get_peer_finished(sslconn->ssl, cb, TLS_CB_MAX);
    }
    else if (strcEQ(type, "CLIENT_TLS_UNIQUE")) {
        l = SSL_get_finished(sslconn->ssl, cb, TLS_CB_MAX);
    }
    else if (strcEQ(type, "SERVER_TLS_SERVER_END_POINT")) {
        x = SSL_get_certificate(sslconn->ssl);
    }
    else if (strcEQ(type, "CLIENT_TLS_SERVER_END_POINT")) {
        x = SSL_get_peer_certificate(sslconn->ssl);
    }
    if (l > 0) {
        preflen = sizeof(TLS_UNIQUE_PREFIX) -1;
        prefix = TLS_UNIQUE_PREFIX;
        data = cb;
    }
    else if (x != NULL) {
        const EVP_MD *md;
        md = EVP_get_digestbynid(X509_get_signature_nid(x));
        /* Override digest as specified by RFC 5929 section 4.1. */
        if (md == NULL || md == EVP_md5() || md == EVP_sha1()) {
            md = EVP_sha256();
        }
        if (!X509_digest(x, md, cb, &l)) {
            return APR_EGENERAL;
        }

        preflen = sizeof(TLS_SERVER_END_POINT_PREFIX) - 1;
        prefix = TLS_SERVER_END_POINT_PREFIX;
        data = cb;
    }
    else {
        return APR_EGENERAL;
    }

    retbuf = apr_palloc(p, preflen + l);
    memcpy(retbuf, prefix, preflen);
    memcpy(&retbuf[preflen], data, l);
    *size = preflen + l;
    *buf = retbuf;

    return APR_SUCCESS;
}

static void trace_nocontext(apr_pool_t *p, const char *file, int line,
                            const char *note)
{
    /*
     * Since we have no request or connection to trace, or any idea
     * from where this routine was called, there's really not much we
     * can do.  If we are not logging everything by way of the
     * EXAMPLE_LOG_EACH constant, do nothing in this routine.
     */

    ap_log_perror(file, line, APLOG_MODULE_INDEX, APLOG_NOTICE, 0, p,
                  APLOGNO(03297) "%s", note);
}

/*
 * Locate our server configuration record for the current server.
 */
static sasl_server_config *our_sconfig(const server_rec *s)
{
    return ap_get_module_config(s->module_config, &arpa2_diasasl_module);
}

/*
 * Locate our directory configuration record for the current request.
 */
static sasl_dir_config *our_dconfig(const request_rec *r)
{
    return (sasl_dir_config *) ap_get_module_config(r->per_dir_config, &arpa2_diasasl_module);
}


/* Callback from diasasl.
 */
static void diasasl_cb (struct diasasl_session *session,
    const int32_t *com_errno,
    char *opt_mechanism_list,
    uint8_t *s2c_token, uint32_t s2c_token_len)
{
    struct diasasl_data *sasl = (struct diasasl_data *) session;
    assert (!sasl->success);
    assert (!sasl->failure);
    if (com_errno == NULL) {
        log_debug ("com_errno IS NULL");
        if (opt_mechanism_list != NULL) {
            assert (der_isnull (&sasl->mechlist));
            assert (der_isnull (&sasl->s2c_token));
            sasl->mechlist.derlen = strlen (opt_mechanism_list);
            sasl->mechlist.derptr = NULL;
            assert (dermem_alloc (sasl, sasl->mechlist.derlen, (void **) &sasl->mechlist.derptr));
            memcpy (sasl->mechlist.derptr, opt_mechanism_list, sasl->mechlist.derlen);
        } else {
            sasl->s2c_token.derptr = NULL;
            if (s2c_token != NULL) {
                assert (dermem_alloc (sasl, s2c_token_len, (void **) &sasl->s2c_token.derptr));
                memcpy (sasl->s2c_token.derptr, s2c_token, s2c_token_len);
            }
            sasl->s2c_token.derlen = s2c_token_len;
        }
    } else {
        log_debug ("*com_errno: %d", *com_errno);
        sasl->s2c_token.derptr = NULL;
        if (s2c_token != NULL) {
            assert (dermem_alloc (sasl, s2c_token_len, (void **) &sasl->s2c_token.derptr));
            memcpy (sasl->s2c_token.derptr, s2c_token, s2c_token_len);
        }
        sasl->s2c_token.derlen = s2c_token_len;
        if (*com_errno == 0) {
            sasl->success = true;
        } else {
            sasl->failure = true;
        }
        sasl->com_errno = *com_errno;
    }
    assert (pthread_mutex_unlock (&sasl->master_action) == 0);
}

/* The master thread for a diasasl node.  It arranges callbacks for all
 * sessions related to the node.  The node's socket should block-wait;
 * otherwise, the master thread would need to poll in a loop and become
 * highly inefficient.
 *
 * When the connection is torn down, its socket is made unavailable to
 * upcoming clients.  This will make them try to get a fresh connection.
 */
static void *diasasl_master (void *node2master) {
    struct diasasl_node *mynode = node2master;
    int32_t com_errno;
    do {
        com_errno = diasasl_process (mynode, false);
        if (com_errno == 0) {
            log_debug ("Unexpected exit from diasasl_process() in the master thread");
        } else {
            diasasl_break (mynode, com_errno);
        }
    } while (com_errno == 0);
    close (mynode->socket);
    mynode->socket = -1;
    return NULL;
}

static void *create_sasl_server_config(apr_pool_t *p, server_rec *s)
{
    sasl_server_config *sconf = apr_pcalloc(p, sizeof *sconf);
    char *note = apr_psprintf(p, "create_sasl_server_config(pool == %pp, sconf = %pp)", p, sconf);
    trace_nocontext(p, __FILE__, __LINE__, note);
    return sconf;
}

static void *create_sasl_dir_config(apr_pool_t *p, char *dirspec)
{
    sasl_dir_config *dconf = apr_pcalloc(p, sizeof *dconf);

    char *note = apr_psprintf(p, "create_sasl_dir_config(pool == %pp, dconf = %pp, dirspec == %s)", p, dconf, dirspec);
    trace_nocontext(p, __FILE__, __LINE__, note);
    return dconf;
}

pcre *re_credentials;
pcre *re_auth_parm;

static int connect_diasasl(apr_pool_t *p, sasl_dir_config *dconf) {
    char *note;

    //
    // Have a TCP connection to the diasasl IP/Port
    struct sockaddr_storage diass;
    memset (&diass, 0, sizeof (diass));
    int sox;

    if (!socket_parse (dconf->diasasl_ip, dconf->diasasl_port, &diass)) {
        note = apr_psprintf(p, "Syntax error in host ip:port setting %s:%s", dconf->diasasl_ip, dconf->diasasl_port);
        trace_nocontext(p, __FILE__, __LINE__, note);
        return -1;
    }
    if (!socket_client (&diass, SOCK_STREAM, &sox)) {
        note = apr_psprintf(p, "Failed to connect to Diameter SASL");
        trace_nocontext(p, __FILE__, __LINE__, note);
        return -1;
    }
    note = apr_psprintf(p, "connect_diasasl %s:%s, sox = %d", dconf->diasasl_ip, dconf->diasasl_port, sox);
    trace_nocontext(p, __FILE__, __LINE__, note);
    return sox;
}

static const char *sasl_server_param(cmd_parms *cmd, void *cfg, const char *val)
{
    char *note;
    sasl_dir_config *dconf = (sasl_dir_config *) cfg;

    note = apr_psprintf(cmd->pool, "sasl_server_param: dconf = %pp", dconf);
    trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
    switch ((long) cmd->info) {
        case cmd_diasasl_ip: {
            dconf->diasasl_ip = apr_pstrdup(cmd->pool, val);
            note = apr_psprintf(cmd->pool, "sasl_server_param: diasasl_ip = %s", dconf->diasasl_ip);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
        }
        break;
        case cmd_diasasl_port: {
            dconf->diasasl_port = apr_pstrdup(cmd->pool, val);
            note = apr_psprintf(cmd->pool, "sasl_server_param: diasasl_port = %s", dconf->diasasl_port);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
        }
        break;
    }
    return NULL;
}

static struct diasasl_data *open_sasl(sasl_dir_config *dconf, struct diasasl_node *diasasl_node)
{
    struct diasasl_data *sasl = NULL;

    //
    // initialise sasl using dermem
    if (dermem_open (sizeof (struct diasasl_data), (void **) &sasl)) {
        sasl->diasasl.callback = diasasl_cb;

        assert(pthread_mutex_init (&sasl->master_action, NULL) == 0);
        assert(pthread_mutex_lock (&sasl->master_action) == 0);
        diasasl_open (diasasl_node, &sasl->diasasl, dconf->diasasl_realm, NULL);  /* Server domain */
        // wait for mech list returned in sasl->mechlist
        assert(pthread_mutex_lock (&sasl->master_action) == 0);
        // store sasl->mechlist in dconf->mechlist
        memcpy(dconf->mechlist, sasl->mechlist.derptr, sasl->mechlist.derlen);
        dconf->mechlist[sasl->mechlist.derlen] = '\0';
        assert(pthread_mutex_destroy(&sasl->master_action));
    }
    return sasl;
}

static const char *sasl_param(cmd_parms *cmd, void *cfg, const char *val)
{
    char *note;

    sasl_dir_config *dconf = (sasl_dir_config *) cfg;
    note = apr_psprintf(cmd->pool, "sasl_param: dconf = %pp", dconf);
    trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
    switch ((long) cmd->info) {
        case cmd_diasasl_realm: {
            dconf->diasasl_realm = apr_pstrdup(cmd->pool, val);
            char *note = apr_psprintf(cmd->pool, "sasl_param: diasasl_realm = %s", dconf->diasasl_realm);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
            note = apr_psprintf(cmd->pool, "sasl_param(pool == %pp)", cmd->pool);
            trace_nocontext(cmd->pool, __FILE__, __LINE__, note);
        }
        break;
    }
    return NULL;
}

static const command_rec sasl_cmds[] = {
    AP_INIT_TAKE1("DiasaslIP", sasl_server_param, (void*)cmd_diasasl_ip, OR_AUTHCFG,
                  "Diasasl IP address"),
    AP_INIT_TAKE1("DiasaslPort", sasl_server_param, (void*)cmd_diasasl_port, OR_AUTHCFG,
                  "Diasasl port"),
    AP_INIT_TAKE1("DiasaslRealm", sasl_param, (void*)cmd_diasasl_realm, OR_AUTHCFG,
                  "Diasasl realm"),
    {NULL}
};

#define OVECCOUNT 30    /* should be a multiple of 3 */
static int ovector[OVECCOUNT];

static char *get_value(apr_pool_t *p, const char *auth_line, int i) {
    int substring_length = ovector[2 * i + 1] - ovector[2 * i];
    const char *substring_start = auth_line + ovector[2 * i];
    char *value = apr_pmemdup(p, substring_start, substring_length + 1);
    value[substring_length] = '\0';
    return value;
}

static apr_hash_t* parse_authorization_header(apr_pool_t *p, const char *authorization)
{
    apr_hash_t* hash = NULL;
    int rc = pcre_exec(
        re_credentials,    /* the compiled pattern */
        NULL,              /* no extra data - we didn't study the pattern */
        authorization,         /* the subject string */
        strlen(authorization), /* the length of the subject */
        0,                 /* start at offset 0 in the subject */
        0,                 /* default options */
        ovector,           /* output vector for substring information */
        OVECCOUNT          /* number of elements in the output vector */
    );
    if (rc >= 0) {
        int start_offset = 0;
        hash = apr_hash_make(p);

        for (;;) {
            rc = pcre_exec(
                re_auth_parm,      /* the compiled pattern */
                NULL,              /* no extra data - we didn't study the pattern */
                authorization,         /* the subject string */
                strlen(authorization), /* the length of the subject */
                start_offset,      /* start at offset 0 in the subject */
                0,                 /* default options */
                ovector,           /* output vector for substring information */
                OVECCOUNT          /* number of elements in the output vector */
            );
            if (rc > 2) {
                char *key = get_value(p, authorization, rc - 2);
                char *value = get_value(p, authorization, rc - 1);
                char *note = apr_psprintf(p, "adding ('%s', '%s')", key, value);
                trace_nocontext(p, __FILE__, __LINE__, note);
                apr_hash_set(hash, key, APR_HASH_KEY_STRING, value);
                start_offset = ovector[1];
            } else {
                trace_nocontext(p, __FILE__, __LINE__, "no (more) auth param matches");
                break;
            }
        }
    } else {
        trace_nocontext(p, __FILE__, __LINE__, "no credentials match");
    }
    return hash;
}

/* not used currently */
static void *merge_auth_sasl_dir_config(apr_pool_t *p, void *basev, void *overridesv)
{
    sasl_dir_config *base = basev;
    sasl_dir_config *overrides = overridesv;
    char *note = apr_psprintf(p, "merge_auth_sasl_dir_config, base->diasasl_realm = %s, overrides->diasasl_realm = %s", base->diasasl_realm, overrides->diasasl_realm);
    trace_nocontext(p, __FILE__, __LINE__, note);
    sasl_dir_config *newconf = create_sasl_dir_config(p, "Merged configuration");
    newconf->diasasl_realm = overrides->diasasl_realm ? overrides->diasasl_realm : base->diasasl_realm;
    return newconf;
}


static void send_www_authenticate(request_rec *r, const char *www_authenticate) {
    char *note = apr_psprintf(r->pool, "WWW-Authenticate = %s", www_authenticate);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);
    apr_table_setn(
        r->err_headers_out,
        (PROXYREQ_PROXY == r->proxyreq) ? "Proxy-Authenticate" : "WWW-Authenticate",
        www_authenticate
    );
}

/* based on https://www.mail-archive.com/dev@apr.apache.org/msg26698.html */
APR_DECLARE(char *) apr_pbase64_encode(apr_pool_t *p, const unsigned char *string, int len)
{
    char *encoded = (char *) apr_palloc(p, apr_base64_encode_len(len));
    apr_base64_encode(encoded, string, len);
    return encoded;
}

APR_DECLARE(unsigned char *) apr_pbase64_decode(apr_pool_t *p, const char *encoded, int *len)
{
    char *decoded = (char *) apr_palloc(p, apr_base64_decode_len(encoded));
    *len = apr_base64_decode(decoded, encoded);
    return decoded;
}

static apr_status_t lock_mutex(apr_thread_mutex_t *mutex, request_rec *r)
{
    apr_status_t rv = apr_thread_mutex_lock(mutex);
    if (rv != APR_SUCCESS) {
        ap_log_rerror(APLOG_MARK, APLOG_ERR, rv, r, "Failed to acquire thread mutex");
    };
    return rv;
}

static void unlock_mutex(apr_thread_mutex_t *mutex, request_rec *r)
{
    apr_status_t rv = apr_thread_mutex_unlock(mutex);
    if (rv != APR_SUCCESS) {
        /* Something is seriously wrong. We need to log it,
         * but it doesn't -– of itself -– invalidate this request.
         */
        ap_log_rerror(APLOG_MARK, APLOG_ERR, rv, r, "Failed to release thread mutex");
    }
}

/* Determine user ID, and check if it really is that user, for HTTP
 * SASL authentication...
 */
static int authenticate_diasasl_user(request_rec *r)
{
    const char *current_auth = ap_auth_type(r);
    trace_nocontext(r->pool, __FILE__, __LINE__, "");
    trace_nocontext(r->pool, __FILE__, __LINE__, "-----------------------------------------------------");
    char *note = apr_psprintf(r->pool, "authenticate_diasasl_user, ap_auth_type = %s", current_auth);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);

    apr_table_unset(r->subprocess_env, "SASL_SECURE");

    if (!current_auth || ap_cstr_casecmp(current_auth, "SASL")) {
        return DECLINED;
    }
    const char *realm = ap_auth_name(r);
    /* We need an authentication realm. */
    if (!realm) {
        ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, APLOGNO(01615)
                      "need AuthName: %s", r->uri);
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    r->ap_auth_type = (char*) current_auth;

    note = apr_psprintf(r->pool, "authenticate_diasasl_user, realm = %s", realm);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);

    sasl_server_config* sconf = our_sconfig(r->server);
    sasl_dir_config *dconf = our_dconfig(r);

    int status_code = HTTP_INTERNAL_SERVER_ERROR;
    /* lock mutex */
#if APR_HAS_THREADS
    if (lock_mutex(sconf->mutex, r) != APR_SUCCESS) {
        return HTTP_SERVICE_UNAVAILABLE;
    }
#endif
    /* get diasasl_node belonging to dconf */
    struct diasasl_node* diasasl_node = apr_hash_get(sconf->connections, dconf, sizeof(dconf));
    if (diasasl_node == NULL) {
        diasasl_node = (struct diasasl_node *) apr_palloc(sconf->pool, sizeof(struct diasasl_node));
        if (diasasl_node != NULL) {
            diasasl_node->socket = -1;
            apr_hash_set(sconf->connections, dconf, sizeof(dconf), diasasl_node);
        }
    }
    /* try to have a diasasl_node->socket >= 0 -- initially and when the master process stopped */
    if (diasasl_node != NULL) {
        if (dconf->diasasl_ip && dconf->diasasl_port && (diasasl_node->socket < 0)) {
            int sox = connect_diasasl(r->pool, dconf);
            char *note = apr_psprintf(r->pool, "authenticate_diasasl_user: connect_diasasl(\"%s\", \"%s\") = %d", dconf->diasasl_ip, dconf->diasasl_port, sox);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            if (sox > 0) {
                diasasl_node->socket = sox;
                diasasl_node_open(diasasl_node);
                pthread_t *diamaster = (pthread_t *) apr_array_push(sconf->diamaster);
                //
                // Start a master process on the dianode before opening sessions on it.
                assert(pthread_create(diamaster, NULL, diasasl_master, (void *) diasasl_node) == 0);

                status_code = OK;
            } else {
                status_code = HTTP_GATEWAY_TIME_OUT;
            }
        } else {
            /* unsure.  without DiaSASL backend, should we signal OK? */
            status_code = OK;
        }
    }
    /* unlock mutex */
#if APR_HAS_THREADS
    unlock_mutex(sconf->mutex, r);
#endif
    if (status_code != OK) {
        return status_code;
    }
    int result;
    status_code = PROXYREQ_PROXY == r->proxyreq
            ? HTTP_PROXY_AUTHENTICATION_REQUIRED
            : HTTP_UNAUTHORIZED;

    note = apr_psprintf(r->pool, "authenticate_diasasl_user: dconf = %pp, dconf->diasasl_ip = %s, dconf->diasasl_port = %s, diasasl_node = %pp, diasasl_node->socket = %d", dconf, dconf->diasasl_ip, dconf->diasasl_port, diasasl_node, diasasl_node->socket);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);
    note = apr_psprintf(r->pool, "uri = %s, dconf = %pp, realm = %s, diasasl_realm = %s", r->uri, dconf, realm, dconf->diasasl_realm);
    trace_nocontext(r->pool, __FILE__, __LINE__, note);

    /* Get the appropriate header */
    const char *authorization = apr_table_get(
        r->headers_in,
        (PROXYREQ_PROXY == r->proxyreq) ? "Proxy-Authorization" : "Authorization"
    );
    if (authorization) {
        note = apr_psprintf(r->pool, "Authorization = %s", authorization);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
        apr_hash_t *hash = parse_authorization_header(r->pool, authorization);
        char *mech = apr_hash_get(hash, "mech", APR_HASH_KEY_STRING);
        note = apr_psprintf(r->pool, "mech = %s", mech);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
        char *c2s_base64 = apr_hash_get(hash, "c2s", APR_HASH_KEY_STRING);
        note = apr_psprintf(r->pool, "c2s_base64 = %s", c2s_base64);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);
        char *s2s_base64 = apr_hash_get(hash, "s2s", APR_HASH_KEY_STRING);
        note = apr_psprintf(r->pool, "s2s_base64 = %s", s2s_base64);
        trace_nocontext(r->pool, __FILE__, __LINE__, note);

        struct diasasl_data *sasl = NULL;
        int s2slen;
        const unsigned char *s2s;
        char *clientin = NULL;
        unsigned clientinlen = 0;
        const char *serverout;
        unsigned serveroutlen;
        if (c2s_base64) {
            clientin = apr_pbase64_decode(r->pool, c2s_base64, &clientinlen);
            note = apr_psprintf(r->pool, "c2s = %.*s", clientinlen, clientin);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
        }
        dercursor c2s = {
            .derptr = clientin,
            .derlen = clientinlen
        };
        unsigned char *tlsbuf = NULL;
        apr_size_t    tlssize = 0;
        if (mech == NULL) {
            if (s2s_base64 != NULL) {
                s2s = apr_pbase64_decode(r->pool, s2s_base64, &s2slen);
                if (s2slen > 0) {
                    sasl = apr_hash_get(sconf->sessions, s2s, s2slen);
                    note = apr_psprintf(r->pool, "sessionid = '%.*s', &sasl->diasasl = %pp", s2slen, s2s, &sasl->diasasl);
                    trace_nocontext(r->pool, __FILE__, __LINE__, note);
                }
            }
        } else {
            if (strlen(mech) <= MAX_MECH_LEN) {
                char *realm;
                uint32_t realmlen;
                char *cbtyp;
                uint32_t cbtyplen;
                bool fit4plain;

                sasl = open_sasl(dconf, diasasl_node);
                if (sasl) {
                    strcpy(sasl->mech, mech);
                    note = apr_psprintf(r->pool, "sasl->mech = '%s'", sasl->mech);
                    trace_nocontext(r->pool, __FILE__, __LINE__, note);
                    s2s = diasasl_sessionid(&sasl->diasasl);
                    s2slen = strlen(s2s);
                    note = apr_psprintf(r->pool, "after open_sasl, sessionid = '%s', &sasl->diasasl = %pp", s2s, &sasl->diasasl);
                    trace_nocontext(r->pool, __FILE__, __LINE__, note);
                    apr_hash_set(sconf->sessions, s2s, APR_HASH_KEY_STRING, &sasl->diasasl);
                    s2s_base64 = apr_pbase64_encode(r->pool, s2s, strlen(s2s));

                    diasasl_mechinfo (mech, clientin, clientinlen, &realm, &realmlen, &cbtyp, &cbtyplen, &fit4plain);
                    note = apr_psprintf(r->pool, "diasasl_mechinfo: fit4plain = %d", fit4plain);
                    trace_nocontext(r->pool, __FILE__, __LINE__, note);
                    if (realm != NULL) {
                        note = apr_psprintf(r->pool, "diasasl_mechinfo: client realm = %.*s", realmlen, realm);
                        trace_nocontext(r->pool, __FILE__, __LINE__, note);
                    }
                    if (cbtyp != NULL) {
                        note = apr_psprintf(r->pool, "diasasl_mechinfo: cbtyp = %.*s", cbtyplen, cbtyp);
                        trace_nocontext(r->pool, __FILE__, __LINE__, note);
                    }
                    //
                    // Setup channel binding information
                    if (ssl_get_tls_cb) {

                        if (ssl_get_tls_cb(r->connection->pool, r->connection, "SERVER_TLS_SERVER_END_POINT", &tlsbuf, &tlssize) == APR_SUCCESS) {
                            log_data("ssl_get_tls_cb", tlsbuf, tlssize, 0);
                            note = apr_psprintf(r->pool, "ssl_get_tls_cb successful");
                        } else {
                            note = apr_psprintf(r->pool, "error calling ssl_get_tls_cb");
                        }
                    } else {
                        note = apr_psprintf(r->pool, "ssl_get_tls_cb is NULL");
                    }
                    trace_nocontext(r->pool, __FILE__, __LINE__, note);
                } else {
                    status_code = HTTP_INTERNAL_SERVER_ERROR;
                }
            }
        }
        if (sasl) {
            sasl->diasasl.chanbind     = tlsbuf;
            sasl->diasasl.chanbind_len = tlssize;
            const char *before = diasasl_sessionid(&sasl->diasasl);
            note = apr_psprintf(r->pool, "before diasasl_send, sessionid '%s'", before);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            assert (pthread_mutex_init (&sasl->master_action, NULL) == 0);
            assert (pthread_mutex_lock (&sasl->master_action) == 0);
            //
            // Make a server step: c2s -> s2c
            diasasl_send (diasasl_node, &sasl->diasasl, mech, c2s.derptr, c2s.derlen);
            // wait for answer
            assert (pthread_mutex_lock (&sasl->master_action) == 0);
            assert(pthread_mutex_destroy(&sasl->master_action));
            const char *after = diasasl_sessionid(&sasl->diasasl);
            note = apr_psprintf(r->pool, "after diasasl_send, sessionid '%s'", after);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            if (sasl->s2c_token.derptr != NULL) {
                note = apr_psprintf(r->pool, "s2c = %.*s", (int) sasl->s2c_token.derlen, sasl->s2c_token.derptr);
                trace_nocontext(r->pool, __FILE__, __LINE__, note);
                char *s2c_base64 = apr_pbase64_encode(r->pool, sasl->s2c_token.derptr, sasl->s2c_token.derlen);
                char *www_authenticate=
                    apr_psprintf(r->pool, "SASL s2c=\"%s\",s2s=\"%s\"", s2c_base64, s2s_base64)
                    ;

                send_www_authenticate(r, www_authenticate);
            }
            if (sasl->success) {
                r->user = apr_psprintf(r->pool, "%s@%s", sasl->diasasl.client_userid, sasl->diasasl.client_domain);
                apr_table_setn(r->subprocess_env, "SASL_SECURE", "yes");
                apr_table_setn(r->subprocess_env, "SASL_MECH", apr_psprintf(r->pool, "%s", sasl->mech));
                apr_table_setn(r->subprocess_env, "SASL_REALM", apr_psprintf(r->pool, "%s", sasl->diasasl.client_domain));
                /*
                Unusable s2s so don't set SASL_S2S
                apr_table_setn(r->subprocess_env, "SASL_S2S", s2s);
                */
                apr_table_unset(r->subprocess_env, "SASL_S2S");
                apr_table_unset(r->subprocess_env, "SASL_S2S_");
                status_code = OK;
            }
            if (sasl->failure) {
                char *com_err_hdr = apr_psprintf(r->pool, "%s (%d)", error_message(sasl->com_errno), sasl->com_errno);
                note = apr_psprintf(r->pool, "com_err: %s", com_err_hdr);
                trace_nocontext(r->pool, __FILE__, __LINE__, note);
                apr_table_setn(
                    r->err_headers_out,
                    "X-Common-Error",
                    com_err_hdr
                );
                goto start;
            }
            if (after == NULL) {
                trace_nocontext(r->pool, __FILE__, __LINE__, "Session complete, cleaning up");
                apr_hash_set(sconf->sessions, before, APR_HASH_KEY_STRING, NULL);
                dermem_close((void **) &sasl);
            }
        }
    } else {
start:
        if (dconf->mechlist[0] == '\0') {
            struct diasasl_data *sasl = open_sasl(dconf, diasasl_node);
            note = apr_psprintf(r->pool, "after open_sasl, sessionid = '%s', &sasl->diasasl = %pp", diasasl_sessionid(&sasl->diasasl), &sasl->diasasl);
            trace_nocontext(r->pool, __FILE__, __LINE__, note);
            diasasl_close(diasasl_node, &sasl->diasasl);
            dermem_close((void **) &sasl);
        }
        // send HTTP SASL mech response
        char *www_authenticate =
                apr_psprintf(r->pool, "SASL mech=\"%s\",realm=\"%s\",s2s=\"\"", dconf->mechlist, realm);
        send_www_authenticate(r, www_authenticate);
    }
    return status_code;
}

static void check_re(apr_pool_t *p, const pcre *re, const char *error, int erroffset)
{
    /* Compilation failed: print the error message and exit */
    if (re == NULL)
    {
        char *note = apr_psprintf(p, "PCRE compilation failed at offset %d: %s", erroffset, error);
        trace_nocontext(p, __FILE__, __LINE__, note);
    }
}


static int close_diasasl_node(void *rec, const void *key, apr_ssize_t klen, const void *value)
{
    sasl_server_config* sconf = (sasl_server_config*) rec;
    apr_pool_t *p = sconf->pool;
    struct diasasl_node *diasasl_node = (struct diasasl_node *) value;
    char *note = apr_psprintf(p, "close_diasasl_node, diasasl_node = %pp", diasasl_node);
    trace_nocontext(p, __FILE__, __LINE__, note);
    if (diasasl_node != NULL) {
        note = apr_psprintf(p, "close_diasasl_node, diasasl_node->socket = %d", diasasl_node->socket);
        trace_nocontext(p, __FILE__, __LINE__, note);
        diasasl_node_close(diasasl_node);
        if (diasasl_node->socket >= 0) {
            close(diasasl_node->socket);
            diasasl_node->socket = -1;
        }
    }
    return 1;
}

static void arpa2_diasasl_fini(sasl_server_config* sconf)
{
    void *res;
    apr_pool_t *p = sconf->pool;
    pthread_t *diamaster;

    trace_nocontext(p, __FILE__, __LINE__, "arpa2_diasasl_fini");
    apr_hash_do(close_diasasl_node, sconf, sconf->connections);
    while (diamaster = (pthread_t *) apr_array_pop(sconf->diamaster))
    {
        int rc = pthread_cancel(*diamaster);
        char *note = apr_psprintf(p, "pthread_cancel, rc = %d", rc);
        trace_nocontext(p, __FILE__, __LINE__, note);
        rc = pthread_join(*diamaster, &res);
        if (rc == 0) {
            if (res == PTHREAD_CANCELED)
                trace_nocontext(p, __FILE__, __LINE__, "thread was canceled");
            else
                trace_nocontext(p, __FILE__, __LINE__, "thread wasn't canceled (shouldn't happen!)");
        }
        else {
            note = apr_psprintf(p, "pthread_cancel, rc = %d", rc);
            trace_nocontext(p, __FILE__, __LINE__, note);
        }
    }
}

static void arpa2_diasasl_init(apr_pool_t *p, server_rec *s)
{
    trace_nocontext(p, __FILE__, __LINE__, "arpa2_diasasl_init");
    const char *error;
    int erroffset;
    char *note;

    re_credentials = pcre_compile(
        RE_CREDENTIALS, /* the pattern */
        0,              /* default options */
        &error,         /* for error message */
        &erroffset,     /* for error offset */
        NULL            /* use default character tables */
    );
    check_re(p, re_credentials, error, erroffset);
    re_auth_parm = pcre_compile(
        RE_AUTH_PARAM,  /* the pattern */
        0,              /* default options */
        &error,         /* for error message */
        &erroffset,     /* for error offset */
        NULL            /* use default character tables */
    );
    check_re(p, re_auth_parm, error, erroffset);

    ssl_get_tls_cb = (ssl_get_tls_cb_t)apr_dynamic_fn_retrieve("ssl_get_tls_cb");
    if (!ssl_get_tls_cb) {
        trace_nocontext(p, __FILE__, __LINE__, "ssl_get_tls_cb not available from mod_ssl");
        ssl_module = ap_find_linked_module("mod_ssl.c");
        char *note = apr_psprintf(p, "ssl_module = %pp", ssl_module);
        trace_nocontext(p, __FILE__, __LINE__, note);
        if (ssl_module) {
            ssl_get_tls_cb = my_ssl_get_tls_cb;
        }
    }
    /* Get the config vector. It already contains the configured
    * values of foo and bar, but the other fields are unset.
    */
    sasl_server_config* sconf = our_sconfig(s);
    /* Derive our own pool from pchild */
    apr_status_t rv = apr_pool_create(&sconf->pool, p);
    if (rv != APR_SUCCESS) {
        ap_log_perror(APLOG_MARK, APLOG_CRIT, rv, p, "Failed to create subpool for my_module");
        return;
    }
    /* Set up a thread mutex for when we need to manipulate the cache */
#if APR_HAS_THREADS
    rv = apr_thread_mutex_create(&sconf->mutex, APR_THREAD_MUTEX_DEFAULT, p);
    if (rv != APR_SUCCESS) {
        ap_log_perror(APLOG_MARK, APLOG_CRIT, rv, p, "Failed to create mutex for my_module");
        return;
    }
#endif
    sconf->connections = apr_hash_make(sconf->pool);
    sconf->sessions = apr_hash_make(sconf->pool);
    sconf->diamaster = apr_array_make(sconf->pool, 10, sizeof(apr_array_header_t *));
    apr_pool_cleanup_register(p, sconf, (void*) arpa2_diasasl_fini, apr_pool_cleanup_null);
    initialize_DiaS_error_table();
}

static void register_hooks(apr_pool_t *p)
{
    ap_hook_child_init(arpa2_diasasl_init, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_check_authn(
        authenticate_diasasl_user,
        NULL,
        NULL,
        APR_HOOK_MIDDLE,
        AP_AUTH_INTERNAL_PER_CONF
    );
}

AP_DECLARE_MODULE(arpa2_diasasl) = {
    STANDARD20_MODULE_STUFF,
    create_sasl_dir_config,        /* create per-directory config structure */
    NULL,                          /* merge per-directory config structures */
    create_sasl_server_config,     /* create per-server config structure */
    NULL,                          /* merge per-server config structures */
    sasl_cmds,                     /* command apr_table_t */
    register_hooks                 /* register hooks */
};
