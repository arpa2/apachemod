<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
-->
# ARPA2 HTTP Diasasl module

> *This module `mod_arpa2_diasasl` implements [HTTP Authentication with SASL](https://tools.ietf.org/html/draft-vanrein-httpauth-sasl)
> using Diasasl as SASL backend.*

## Diasasl

[Diasasl](https://tools.ietf.org/html/draft-vanrein-diameter-sasl-04#appendix-A) is an ASN.1 module, where this apache module acts as a Diasasl client and communicates with a Diasasl server. ARPA2 implements a [freeDiameter](http://www.freediameter.net/trac/) [extension](https://gitlab.com/arpa2/kip/-/blob/master/src/diasasl_cli.c), which acts as as Diasasl server, and is loaded as an extension into a freeDiameter client. The freeDiameter client converts the diasasl messages to the diameter protocol and sends them to a Diameter server.

## Directives
This module uses apache's [mod_authn_core](https://httpd.apache.org/docs/2.4/mod/mod_authn_core.html)  and [mod_authz_core](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html) modules and introduces a new AuthType "SASL". The [Context](https://httpd.apache.org/docs/2.4/mod/directive-dict.html#Context) of these directives is "directory" or ".htaccess".
|Name|Description|
|--|--|
|`AuthType`|Should be `AuthType SASL`
|`AuthName`|This directive sets the name of the authorization realm for a directory. This realm is given to the client so that the user knows which username and password to send. `AuthName` takes a single argument; if the realm name contains spaces, it must be enclosed in quotation marks. It must be accompanied by [AuthType](https://httpd.apache.org/docs/2.4/mod/mod_authn_core.html#authtype) and [Require](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html#require) directives. Eg. `Authname "Diameter SASL realm"`
|`Require`|See mod_authz_core [Require](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html#require) directive, eg. `Require valid-user`
|`DiasaslRealm`|Realm of the Diasasl server eg. `DiasaslRealm "arpa2.net"`.|
|`DiasaslIP`|IP address of the Diasasl server eg. `DiasaslIP ::1`|
|`DiasaslPort`|Port number of the Diasasl server eg. `DiasaslPort 3869`|

## Environment variables set on successful authentication
Taken from [Appendix A. HTTP Server Environment Variables](https://tools.ietf.org/id/draft-vanrein-httpauth-sasl-05.html#name-http-server-environment-var)
|Name|Description|
|--|--|
|`SASL_SECURE`|is only "yes" (without the quotes) when a client is authenticated to the current resource. It never has another value; it is simply undefined when not secured by SASL|
|`SASL_REALM`|is the realm for which the secure exchange succeeded. A realm is not always used, because sites only need it when there are more than one in the same name space. When undefined in the SASL flow, this variable will not be set.|
|`REMOTE_USER`|is the client identity as confirmed through SASL authentication. Its content is formatted like an email address, and includes a domain name. That domain need not be related to the web server; it is possible for a web server to welcome foreign clients.|
|`SASL_MECH`|indicates the mechanism used, and is one of the standardised SASL mechanism names. It may be used to detect the level of security.|
|`SASL_S2S`|currently unset|
|`SASL_S2S_`|currently unset|
